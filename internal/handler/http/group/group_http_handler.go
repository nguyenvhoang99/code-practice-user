package group_handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/utils"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/constants"
	http_handler_shared "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/shared"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	group_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/group"
)

type HttpHandler struct {
	groupService group_service.IService
}

func NewHttpHandler(groupService group_service.IService) *HttpHandler {
	return &HttpHandler{
		groupService: groupService,
	}
}

func (hh *HttpHandler) CreateGroupV1(ctx *gin.Context) {
	group := &model.Group{}
	err := ctx.ShouldBindJSON(group)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}
	group.GroupId = ""

	groupId, err := hh.groupService.CreateGroup(ctx.Request.Context(), group, ctx.GetString(http_handler_shared.HttpUserIdCtxKey))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure, "traceId": http_handler_shared.GetHttpTraceId(ctx.Request.Context())})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": gin.H{"groupId": groupId}})
}

func (hh *HttpHandler) AddGroupMemberV1(ctx *gin.Context) {
	if ctx.Param("group_id") == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"invalid": "group_id"})
		return
	}
	if ctx.Param("member_id") == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"invalid": "member_id"})
		return
	}
	if ctx.Query("role") == "" ||
		!utils.Contains(constants.Group_AllRoles, ctx.Query("role")) {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"invalid": "role"})
		return
	}

	role, err := hh.groupService.CheckUserInGroup(ctx.Request.Context(), ctx.Param("group_id"), ctx.GetString(http_handler_shared.HttpUserIdCtxKey))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure, "traceId": http_handler_shared.GetHttpTraceId(ctx.Request.Context())})
		return
	}
	if role != constants.Group_RoleAdmin &&
		role != constants.Group_RoleTeacher &&
		role != constants.Group_RoleLecturer {
		ctx.AbortWithStatus(http.StatusPreconditionFailed)
		return
	}

	ok, err := hh.groupService.AddUserToGroup(ctx.Request.Context(), ctx.Param("group_id"), ctx.Param("member_id"), ctx.Query("role"))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure, "traceId": http_handler_shared.GetHttpTraceId(ctx.Request.Context())})
		return
	}
	if !ok {
		ctx.Status(http.StatusNotModified)
		return
	}
	ctx.Status(http.StatusOK)
}

func (hh *HttpHandler) GetGroupMemberProfilesV1(ctx *gin.Context) {
	memberProfiles, err := hh.groupService.QueryUserProfilesInGroup(ctx.Request.Context(), ctx.Param("group_id"),
		getHttpQueryProfileOption(ctx))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure, "traceId": http_handler_shared.GetHttpTraceId(ctx.Request.Context())})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": memberProfiles})
}

func getHttpQueryProfileOption(ctx *gin.Context) *model.QueryProfileOption {
	return &model.QueryProfileOption{
		WithBasicProfile:       ctx.Query("with_basic_profile") == "true",
		WithAllBindingProfiles: ctx.Query("with_all_binding_profiles") == "true",
		BindingAppNames:        ctx.QueryArray("binding_app_names"),
	}
}
