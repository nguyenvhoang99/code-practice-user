package group_handler_fx

import (
	"go.uber.org/fx"

	group_http_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/group"
)

var HttpHandlerModule = fx.Provide(group_http_handler.NewHttpHandler)
