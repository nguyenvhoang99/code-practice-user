package auth_handler

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/constants"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	auth_repostiory "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/auth"
	profile_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/profile"
	session_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/session"
)

var (
	ErrInvalidAuthBindingAppName = &custom_err.InvalidErr{Err: "invalid auth bindingAppName"}
	ErrInvalidAuthArg            = &custom_err.InvalidErr{Err: "invalid auth arg"}
	ErrEmptyAuthEmail            = &custom_err.InvalidErr{Err: "empty auth email"}
	ErrEmptyAuthPhone            = &custom_err.InvalidErr{Err: "empty auth phone"}
	ErrEmptyAuthPassword         = &custom_err.InvalidErr{Err: "empty auth password"}
	ErrEmptyAuthOneTimePassword  = &custom_err.InvalidErr{Err: "empty auth one time password"}
	ErrInvalidAuthData           = &custom_err.InvalidErr{Err: "invalid auth data"}
	ErrInvalidLoginInfo          = &custom_err.InvalidErr{Err: "invalid login info"}
)

type HttpHandler struct {
	authCache      auth_repostiory.ICache
	authDatabase   auth_repostiory.IDatabase
	profileService profile_service.IService
	sessionService session_service.IService
}

func NewHttpHandler(authDatabase auth_repostiory.IDatabase, profileService profile_service.IService, sessionService session_service.IService) *HttpHandler {
	return &HttpHandler{
		authDatabase:   authDatabase,
		profileService: profileService,
		sessionService: sessionService,
	}
}

func (hh *HttpHandler) LoginWithBindingApp(ctx *gin.Context) {
	var userId string

	var err error
	switch ctx.Query("binding_app_name") {
	case constants.Profile_GoogleBindingAppName:
		userId, err = hh.handleLoginWithGoogle(ctx)
	default:
		err = ErrInvalidAuthBindingAppName
	}
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure})
		return
	}

	sessionId, err := hh.sessionService.CreateSession(ctx.Request.Context(), userId, getHttpLoginInfoFromQueryParams(ctx))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"data": gin.H{"session_id": sessionId}})
}

func (hh *HttpHandler) Login(ctx *gin.Context) {
	authInfo := make(map[string]string)
	if err := json.NewDecoder(ctx.Request.Body).Decode(&authInfo); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"failure": "invalid auth arg"})
		return
	}

	var userId string

	var err error
	if authInfo["email"] != "" {
		userId, err = hh.handleLoginWithEmailAndPassword(ctx, authInfo["email"], authInfo["password"])
	} else if authInfo["phone"] != "" {
		userId, err = hh.handleLoginWithPhoneAndOneTimePassword(ctx, authInfo["phone"], authInfo["one_time_password"])
	} else {
		err = ErrInvalidAuthArg
	}
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure})
		return
	}

	sessionId, err := hh.sessionService.CreateSession(ctx.Request.Context(), userId,
		getHttpLoginInfoFromHeaders(ctx))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"data": gin.H{"session_id": sessionId}})
}

func (hh *HttpHandler) Logout(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
}

func (hh *HttpHandler) handleLoginWithGoogle(ctx *gin.Context) (string, error) {
	// Extract state & code from ctx.Query, then token from code, then userInfo from token, then email from userInfo
	email := ""

	profile, err := hh.profileService.QueryProfileIdsByBindingAppEmail(ctx.Request.Context(), constants.Profile_GoogleBindingAppName, email)
	if err != nil {
		return "", err
	}
	return profile.UserId, nil
}

func (hh *HttpHandler) handleLoginWithEmailAndPassword(ctx *gin.Context, email, password string) (string, error) {
	if email == "" {
		return "", ErrEmptyAuthEmail
	}
	if password == "" {
		return "", ErrEmptyAuthPassword
	}

	ok, err := hh.authDatabase.VerifyEmailAndPassword(ctx.Request.Context(), email, password)
	if err != nil {
		return "", err
	}
	if !ok {
		return "", ErrInvalidAuthData
	}

	profile, err := hh.profileService.QueryProfileByEmail(ctx.Request.Context(), email, &model.QueryProfileOption{})
	if err != nil {
		return "", err
	}
	return profile.UserId, nil
}

func (hh *HttpHandler) handleLoginWithPhoneAndOneTimePassword(ctx *gin.Context, phone, oneTimePassword string) (string, error) {
	if phone == "" {
		return "", ErrEmptyAuthPhone
	}
	if oneTimePassword == "" {
		return "", ErrEmptyAuthOneTimePassword
	}

	ok, err := hh.authCache.VerifyPhoneAndOneTimePassword(ctx.Request.Context(), phone, oneTimePassword)
	if err != nil {
		return "", err
	}
	if !ok {
		return "", ErrInvalidAuthData
	}

	profile, err := hh.profileService.QueryProfileByPhone(ctx.Request.Context(), phone, &model.QueryProfileOption{})
	if err != nil {
		return "", err
	}
	return profile.UserId, nil
}

func getHttpLoginInfoFromQueryParams(ctx *gin.Context) *model.LoginInfo {
	return &model.LoginInfo{
		UserIP:    "",
		UserAgent: "",
		Platform:  "",
		Version:   "",
		Device:    "",
	}
}

func getHttpLoginInfoFromHeaders(ctx *gin.Context) *model.LoginInfo {
	return &model.LoginInfo{
		UserIP:    "",
		UserAgent: "",
		Platform:  "",
		Version:   "",
		Device:    "",
	}
}
