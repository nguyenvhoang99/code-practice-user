package http_handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"

	"gitlab.com/nguyenvhoang99/common-pkg/pkg/app"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	auth_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/auth"
	group_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/group"
	profile_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/profile"
	http_handler_shared "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/shared"
	session_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/session"
)

const (
	profileV1Path = "/api/v1/profiles"
	groupV1Path   = "/api/v1/groups"
)

func NewHandler(appConfig *app.Config,
	authHttpHandler *auth_handler.HttpHandler,
	profileHttpHandler *profile_handler.HttpHandler,
	groupHttpHandler *group_handler.HttpHandler,
	sessionService session_service.IService,
	observer observer.IObserver) *http.ServeMux {
	httpHandler := http.NewServeMux()

	ginEngine := gin.Default()

	ginEngine.Use(otelgin.Middleware(appConfig.GetServiceName()))
	ginEngine.Use(http_handler_shared.MiddlewareHTTPLogReqAndRes(observer))
	ginEngine.Use(http_handler_shared.MiddlewareHTTPRecordThroughputAndLatency(observer))
	ginEngine.Use(http_handler_shared.MiddlewareHTTPRecordError(observer))

	ginEngine.GET("/login_callback/:binding_app_name", authHttpHandler.LoginWithBindingApp)
	ginEngine.POST("/login", authHttpHandler.Login)
	ginEngine.POST("/logout", authHttpHandler.Logout)

	profileV1Group := ginEngine.Group(profileV1Path)
	profileV1Group.Use(http_handler_shared.MiddlewareHTTPAuthenticationByUserToken(sessionService, observer))
	// User APIs
	// v1
	// Profile
	profileV1Group.GET("", profileHttpHandler.GetProfileV1)

	groupV1Group := ginEngine.Group(groupV1Path)
	groupV1Group.Use(http_handler_shared.MiddlewareHTTPAuthenticationByUserToken(sessionService, observer))
	// Group APIs
	// v1
	groupV1Group.POST("", groupHttpHandler.CreateGroupV1)
	groupV1Group.POST("/:group_id/members/:member_id", groupHttpHandler.AddGroupMemberV1)
	groupV1Group.GET("/:group_id/members/profiles", groupHttpHandler.GetGroupMemberProfilesV1)

	httpHandler.HandleFunc("/", func(reswr http.ResponseWriter, req *http.Request) {
		ginEngine.ServeHTTP(reswr, req)
	})

	return httpHandler
}
