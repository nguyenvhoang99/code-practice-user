package http_handler_shared

import (
	"bytes"

	"github.com/gin-gonic/gin"
)

type ginCustomResponseWriter struct {
	gin.ResponseWriter
	StatusCode int
	BodyBuffer *bytes.Buffer
}

func (crw *ginCustomResponseWriter) Write(body []byte) (int, error) {
	return crw.BodyBuffer.Write(body)
}
