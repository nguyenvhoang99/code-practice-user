package http_handler_shared

import (
	"context"

	"go.opentelemetry.io/otel/trace"
	"go.opentelemetry.io/otel/trace/noop"
)

func GetHttpTraceId(ctx context.Context) string {
	span := trace.SpanFromContext(ctx)
	if _, ok := span.(noop.Span); ok || !span.SpanContext().TraceID().IsValid() {
		return ""
	}
	return span.SpanContext().TraceID().String()
}
