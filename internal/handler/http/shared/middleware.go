package http_handler_shared

import (
	"bytes"
	"errors"
	"go.uber.org/zap"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"
	pkg_shared "gitlab.com/nguyenvhoang99/common-pkg/pkg/server/http/shared"

	session_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/session"
)

func MiddlewareHTTPLogReqAndRes(observer observer.IObserver) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if strings.HasPrefix(ctx.Request.URL.Path, pkg_shared.HttpHealthServicePrefix) {
			ctx.Next()
			return
		}

		reqBody := ""
		reqBodyByteArr, err := io.ReadAll(ctx.Request.Body)
		if err != nil {
			observer.LogError(ctx.Request.Context(), "failed to read request body", zap.Error(err))
		} else {
			reqBody = string(reqBodyByteArr)
			ctx.Request.Body = io.NopCloser(bytes.NewBuffer(reqBodyByteArr))
		}

		responseWriter := ctx.Writer

		customResponseWriter := &ginCustomResponseWriter{
			ResponseWriter: responseWriter,
			BodyBuffer:     &bytes.Buffer{},
		}
		ctx.Writer = customResponseWriter

		ctx.Next()

		pkg_shared.LogHttpServerReqAndRes(ctx.Request.Context(), observer,
			ctx.Request.Method, ctx.Request.RequestURI, reqBody,
			customResponseWriter.Status(), customResponseWriter.BodyBuffer.String())

		_, err = io.Copy(responseWriter, customResponseWriter.BodyBuffer)
		if err != nil {
			observer.LogError(ctx.Request.Context(), "failed to write response body", zap.Error(err))
		}
	}
}

func MiddlewareHTTPRecordThroughputAndLatency(observer observer.IObserver) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if strings.HasPrefix(ctx.Request.URL.Path, pkg_shared.HttpHealthServicePrefix) {
			ctx.Next()
			return
		}

		pkg_shared.RecordHttpServerThroughput(ctx.Request.Context(), observer, ctx.Request.Method, ctx.FullPath())

		startTime := time.Now()

		ctx.Next()

		pkg_shared.RecordHttpServerLatency(ctx.Request.Context(), observer, ctx.Request.Method, ctx.FullPath(), startTime)
	}
}

func MiddlewareHTTPRecordError(observer observer.IObserver) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if strings.HasPrefix(ctx.Request.URL.Path, pkg_shared.HttpHealthServicePrefix) {
			ctx.Next()
			return
		}

		ctx.Next()

		if statusCode := ctx.Writer.Status(); statusCode != http.StatusOK {
			pkg_shared.RecordHttpError(ctx.Request.Context(), observer, ctx.Request.Method, ctx.FullPath(), statusCode)
		}
	}
}

func MiddlewareHTTPAuthenticationByUserToken(sessionService session_service.IService, observer observer.IObserver) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		sessionId := strings.TrimSpace(strings.Replace(ctx.GetHeader("Authorization"), "Bearer", "", 1))
		if sessionId == "" {
			ctx.AbortWithStatusJSON(http.StatusForbidden, gin.H{"invalid": "header authorization"})
			return
		}

		session, err := sessionService.GetSession(ctx.Request.Context(), sessionId)
		if err != nil {
			if errors.Is(err, session_service.ErrEmptySessionId) ||
				errors.Is(err, session_service.ErrInvalidSessionId) ||
				errors.Is(err, session_service.ErrNotFoundSessionId) {
				ctx.AbortWithStatusJSON(http.StatusForbidden, gin.H{"invalid": "userToken"})
				return
			}
			if errors.Is(err, session_service.ErrAlreadyExpiredSessionId) {
				ctx.AbortWithStatusJSON(http.StatusForbidden, gin.H{"already expired": "userToken"})
				return
			}
			ctx.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		ctx.Set(HttpUserIdCtxKey, session.UserId)

		ctx.Next()
	}
}
