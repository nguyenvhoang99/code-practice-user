package profile_handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"

	http_handler_shared "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/shared"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	profile_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/profile"
)

type HttpHandler struct {
	profileService profile_service.IService
}

func NewHttpHandler(profileService profile_service.IService) *HttpHandler {
	return &HttpHandler{
		profileService: profileService,
	}
}

func (hh *HttpHandler) GetProfileV1(ctx *gin.Context) {
	profile, err := hh.profileService.QueryProfileByUserId(ctx.Request.Context(), ctx.GetString(http_handler_shared.HttpUserIdCtxKey),
		getHttpQueryProfileOption(ctx))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure, "traceId": http_handler_shared.GetHttpTraceId(ctx.Request.Context())})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": profile})
}

func getHttpQueryProfileOption(ctx *gin.Context) *model.QueryProfileOption {
	return &model.QueryProfileOption{
		WithBasicProfile:       ctx.Query("with_basic_profile") == "true",
		WithAllBindingProfiles: ctx.Query("with_all_binding_profiles") == "true",
		BindingAppNames:        ctx.QueryArray("binding_app_names"),
	}
}
