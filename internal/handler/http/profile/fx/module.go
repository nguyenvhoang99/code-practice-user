package profile_handler_fx

import (
	"go.uber.org/fx"

	profile_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/profile"
)

var HandlerModule = fx.Provide(profile_handler.NewHttpHandler)
