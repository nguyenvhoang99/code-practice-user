package session_handler_fx

import (
	"go.uber.org/fx"

	session_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/session"
)

var HandlerModule = fx.Provide(session_handler.NewHttpHandler)
