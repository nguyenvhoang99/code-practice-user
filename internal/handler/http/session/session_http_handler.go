package session_handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"

	http_handler_shared "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/shared"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	session_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/session"
)

type HttpHandler struct {
	sessionService session_service.IService
}

func NewHttpHandler(sessionService session_service.IService) *HttpHandler {
	return &HttpHandler{
		sessionService: sessionService,
	}
}

func (hh *HttpHandler) CreateSessionV1(ctx *gin.Context) {
	sessionId, err := hh.sessionService.CreateSession(ctx.Request.Context(), ctx.Param("user_id"),
		getHttpLoginInfoFromHeaders(ctx))
	if err != nil {
		status, failure := custom_err.TranslateToHttpError(err)
		ctx.AbortWithStatusJSON(status, gin.H{"failure": failure, "traceId": http_handler_shared.GetHttpTraceId(ctx.Request.Context())})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": gin.H{"sessionId": sessionId}})
}

func getHttpLoginInfoFromHeaders(ctx *gin.Context) *model.LoginInfo {
	return &model.LoginInfo{
		UserIP:    "",
		UserAgent: "",
		Platform:  "",
		Version:   "",
		Device:    "",
	}
}
