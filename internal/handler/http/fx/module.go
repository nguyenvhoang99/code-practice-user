package http_handler_fx

import (
	"go.uber.org/fx"

	http_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http"
)

var HandlerModule = fx.Provide(http_handler.NewHandler)
