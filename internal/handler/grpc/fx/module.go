package grpc_handler_fx

import (
	"go.uber.org/fx"

	grpc_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc"
)

var HandlerModule = fx.Provide(grpc_handler.NewGrpcHandler)
