package session_handler

import (
	"context"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"

	session_grpc "gitlab.com/nguyenvhoang99/code-practice-user/api/grpc/session"
	session_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/session"
)

type GrpcHandler struct {
	sessionService session_service.IService
	session_grpc.UnimplementedSessionServer
}

func NewGrpcHandler(sessionService session_service.IService) *GrpcHandler {
	return &GrpcHandler{
		sessionService: sessionService,
	}
}

func (gh *GrpcHandler) GetSession(ctx context.Context, req *session_grpc.GetSessionRequest) (*session_grpc.GetSessionResponse, error) {
	session, err := gh.sessionService.GetSession(ctx, req.GetSessionId())
	if err != nil {
		return nil, custom_err.TranslateToGrpcError(err)
	}
	return &session_grpc.GetSessionResponse{
		UserId: session.UserId,
		LoginInfo: &session_grpc.LoginInfo{
			UserIp:    session.LoginInfo.UserIP,
			UserAgent: session.LoginInfo.UserAgent,
			Platform:  session.LoginInfo.Platform,
			Version:   session.LoginInfo.Version,
			Device:    session.LoginInfo.Device,
		},
		CreatedAt: session.CreatedAt,
		ExpiredAt: session.ExpiredAt,
	}, nil
}
