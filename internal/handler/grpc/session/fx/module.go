package session_grpc_handler_fx

import (
	"go.uber.org/fx"

	session_grpc_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc/session"
)

var HandlerModule = fx.Provide(session_grpc_handler.NewGrpcHandler)
