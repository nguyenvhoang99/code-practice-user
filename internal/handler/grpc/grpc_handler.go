package grpc_handler

import (
	grpc_server "gitlab.com/nguyenvhoang99/common-pkg/pkg/server/grpc"

	profile_api "gitlab.com/nguyenvhoang99/code-practice-user/api/grpc/profile"
	session_api "gitlab.com/nguyenvhoang99/code-practice-user/api/grpc/session"
	profile_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc/profile"
	session_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc/session"
)

func NewGrpcHandler(sessionGrpcHandler *session_handler.GrpcHandler,
	profileGrpcHandler *profile_handler.GrpcHandler) []*grpc_server.Service {
	grpcServices := make([]*grpc_server.Service, 0)
	grpcServices = append(grpcServices,
		&grpc_server.Service{
			ServiceDesc: &session_api.Session_ServiceDesc,
			ServiceImpl: sessionGrpcHandler,
		},
		&grpc_server.Service{
			ServiceDesc: &profile_api.Profile_ServiceDesc,
			ServiceImpl: profileGrpcHandler,
		})
	return grpcServices
}
