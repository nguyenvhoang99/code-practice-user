package profile_grpc_handler_fx

import (
	"go.uber.org/fx"

	profile_grpc_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc/profile"
)

var HandlerModule = fx.Provide(profile_grpc_handler.NewGrpcHandler)
