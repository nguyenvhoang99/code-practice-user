package profile_handler

import (
	"context"

	grpc_codes "google.golang.org/grpc/codes"
	grpc_status "google.golang.org/grpc/status"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"

	profile_grpc "gitlab.com/nguyenvhoang99/code-practice-user/api/grpc/profile"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	profile_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/profile"
)

const maxQueryProfilesByIdsLen = 20

type GrpcHandler struct {
	profileService profile_service.IService
	profile_grpc.UnimplementedProfileServer
}

func NewGrpcHandler(profileService profile_service.IService) *GrpcHandler {
	return &GrpcHandler{
		profileService: profileService,
	}
}

func (gh *GrpcHandler) QueryProfileByUserId(ctx context.Context, req *profile_grpc.QueryProfileByUserIdRequest) (*profile_grpc.QueryProfileResponse, error) {
	profile, err := gh.profileService.QueryProfileByUserId(ctx, req.GetUserId(), mapQueryProfileOption(req.GetQueryOption()))
	return baseHandleQueryProfile(profile, err)
}

func (gh *GrpcHandler) QueryProfileByEmail(ctx context.Context, req *profile_grpc.QueryProfileByEmailRequest) (*profile_grpc.QueryProfileResponse, error) {
	profile, err := gh.profileService.QueryProfileByEmail(ctx, req.GetEmail(), mapQueryProfileOption(req.GetQueryOption()))
	return baseHandleQueryProfile(profile, err)
}

func (gh *GrpcHandler) QueryProfileByPhone(ctx context.Context, req *profile_grpc.QueryProfileByPhoneRequest) (*profile_grpc.QueryProfileResponse, error) {
	profile, err := gh.profileService.QueryProfileByPhone(ctx, req.GetPhone(), mapQueryProfileOption(req.GetQueryOption()))
	return baseHandleQueryProfile(profile, err)
}

func (gh *GrpcHandler) QueryProfilesByUserIds(ctx context.Context, req *profile_grpc.QueryProfilesByUserIdsRequest) (*profile_grpc.QueryProfilesResponse, error) {
	if err := validateProfileIds(req.GetUserIds()); err != nil {
		return nil, err
	}
	profiles, err := gh.profileService.QueryProfilesByUserIds(ctx, req.GetUserIds(), mapQueryProfileOption(req.GetQueryOption()))
	return baseHandleQueryProfiles(profiles, err)
}

func (gh *GrpcHandler) QueryProfilesByEmails(ctx context.Context, req *profile_grpc.QueryProfilesByEmailsRequest) (*profile_grpc.QueryProfilesResponse, error) {
	if err := validateProfileIds(req.GetEmails()); err != nil {
		return nil, err
	}
	profiles, err := gh.profileService.QueryProfilesByEmails(ctx, req.GetEmails(), mapQueryProfileOption(req.GetQueryOption()))
	return baseHandleQueryProfiles(profiles, err)
}

func (gh *GrpcHandler) QueryProfilesByPhones(ctx context.Context, req *profile_grpc.QueryProfilesByPhonesRequest) (*profile_grpc.QueryProfilesResponse, error) {
	if err := validateProfileIds(req.GetPhones()); err != nil {
		return nil, err
	}
	profiles, err := gh.profileService.QueryProfilesByPhones(ctx, req.GetPhones(), mapQueryProfileOption(req.GetQueryOption()))
	return baseHandleQueryProfiles(profiles, err)
}

func baseHandleQueryProfile(profile *model.Profile, err error) (*profile_grpc.QueryProfileResponse, error) {
	if err != nil {
		return nil, custom_err.TranslateToGrpcError(err)
	}
	res := &profile_grpc.QueryProfileResponse{
		UserId: profile.UserId,
		Email:  profile.Email,
		Phone:  profile.Phone,
	}
	if profile.BasicProfile != nil {
		res.BasicProfile = &profile_grpc.BasicProfile{
			Name:   profile.BasicProfile.Name,
			Avatar: profile.BasicProfile.Avatar,
		}
	}
	if len(profile.BindingProfiles) > 0 {
		bindingProfiles := make(map[string]*profile_grpc.BindingProfile)
		for bindingAppName, bindingProfile := range profile.BindingProfiles {
			bindingProfiles[bindingAppName] = &profile_grpc.BindingProfile{
				AppName:   bindingProfile.AppName,
				AppUserId: bindingProfile.AppUserId,
				AppEmail:  bindingProfile.AppEmail,
				AppPhone:  bindingProfile.AppPhone,
			}
		}
		res.BindingProfiles = bindingProfiles
	}
	return res, err
}

func baseHandleQueryProfiles(profiles map[string]*model.Profile, err error) (*profile_grpc.QueryProfilesResponse, error) {
	if err != nil {
		return nil, custom_err.TranslateToGrpcError(err)
	}
	res := &profile_grpc.QueryProfilesResponse{
		Profiles: make(map[string]*profile_grpc.QueryProfileResponse),
	}
	for key, profile := range profiles {
		res.Profiles[key] = &profile_grpc.QueryProfileResponse{
			UserId: profile.UserId,
			Email:  profile.Email,
			Phone:  profile.Phone,
		}
		if profile.BasicProfile != nil {
			res.Profiles[key].BasicProfile = &profile_grpc.BasicProfile{
				Name:   profile.BasicProfile.Name,
				Avatar: profile.BasicProfile.Avatar,
			}
		}
		if len(profile.BindingProfiles) > 0 {
			bindingProfiles := make(map[string]*profile_grpc.BindingProfile)
			for bindingAppName, bindingProfile := range profile.BindingProfiles {
				bindingProfiles[bindingAppName] = &profile_grpc.BindingProfile{
					AppName:   bindingProfile.AppName,
					AppUserId: bindingProfile.AppUserId,
					AppEmail:  bindingProfile.AppEmail,
					AppPhone:  bindingProfile.AppPhone,
				}
			}
			res.Profiles[key].BindingProfiles = bindingProfiles
		}
	}
	return res, err
}

func validateProfileIds(profileIds []string) error {
	if len(profileIds) > maxQueryProfilesByIdsLen {
		return grpc_status.Errorf(grpc_codes.InvalidArgument, "len(profileIds) should be <= %d", maxQueryProfilesByIdsLen)
	}
	return nil
}

func mapQueryProfileOption(queryOption *profile_grpc.QueryProfileOption) *model.QueryProfileOption {
	return &model.QueryProfileOption{
		WithBasicProfile:       queryOption.GetWithBasicProfile(),
		WithAllBindingProfiles: queryOption.GetWithAllBindingProfiles(),
		BindingAppNames:        filterEmptyValues(queryOption.GetBindingAppNames()),
	}
}

func filterEmptyValues(values []string) []string {
	nonEmptyValues := make([]string, 0)
	for _, value := range values {
		if value == "" {
			continue
		}
		nonEmptyValues = append(nonEmptyValues, value)
	}
	return nonEmptyValues
}
