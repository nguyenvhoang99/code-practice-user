package session_consumer

//
//import (
//	"context"
//
//	"go.uber.org/zap"
//
//	kafka_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/kafka"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
//
//	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/configs/auth"
//)
//
//type AnnounceLoginKafkaConsumerService struct {
//	configs *session_config.Config
//	logger logger.ILogger
//}
//
//func NewAnnounceLoginKafkaConsumerService(configs *session_config.Config, logger logger.ILogger) kafka_broker.IConsumerService {
//	return &AnnounceLoginKafkaConsumerService{
//		configs: configs,
//		logger: logger,
//	}
//}
//
//func (alkcs *AnnounceLoginKafkaConsumerService) Topic() string {
//	return alkcs.configs.AnnounceLoginKafkaTopic
//}
//
//func (alkcs *AnnounceLoginKafkaConsumerService) Handle() func(ctx context.Context, partition int32, offset int64, key, value []byte, headers map[string][]byte) {
//	return func(ctx context.Context, partition int32, offset int64, key, value []byte, headers map[string][]byte) {
//		alkcs.logger.InfoCtx(ctx, "consumed for announcing login",
//			zap.Any("message", value))
//	}
//}
