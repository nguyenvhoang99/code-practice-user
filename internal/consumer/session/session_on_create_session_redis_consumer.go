package session_consumer

//
//import (
//	"context"
//
//	"go.uber.org/zap"
//
//	redis_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/redis"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
//
//	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/configs/auth"
//)
//
//type OnCreateSessionRedisConsumerService struct {
//	configs *session_config.Config
//	logger logger.ILogger
//}
//
//func NewOnCreateSessionRedisConsumerService(configs *session_config.Config, logger logger.ILogger) redis_broker.IConsumerService {
//	return &OnCreateSessionRedisConsumerService{
//		configs: configs,
//		logger: logger,
//	}
//}
//
//func (ocsrcs *OnCreateSessionRedisConsumerService) Channel() string {
//	return ocsrcs.configs.OnCreateSessionRedisChannel
//}
//
//func (ocsrcs *OnCreateSessionRedisConsumerService) Handle() func(ctx context.Context, payload string) {
//	return func(ctx context.Context, payload string) {
//		ocsrcs.logger.InfoCtx(ctx, "consumed for on create auth",
//			zap.Any("message", payload))
//	}
//}
