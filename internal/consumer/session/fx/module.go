package session_consumer_fx

//
//import (
//	"go.uber.org/fx"
//
//	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/configs/auth"
//	session_consumer "gitlab.com/nguyenvhoang99/code-practice-user/internal/consumer/auth"
//
//	kafka_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/kafka"
//	kafka_broker_fx "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/kafka/fx"
//	redis_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/redis"
//	redis_broker_fx "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/redis/fx"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
//)
//
//var ConsumerServiceModule = fx.Provide(
//	redis_broker_fx.AsConsumerServiceForFx(NewOnCreateSessionConsumerServiceForFx),
//	kafka_broker_fx.AsConsumerServiceForFx(NewAnnounceLoginConsumerServiceForFx),
//)
//
//func NewOnCreateSessionConsumerServiceForFx(configs *session_config.Config, logger logger.ILogger) redis_broker.IConsumerService {
//	return session_consumer.NewOnCreateSessionRedisConsumerService(configs, logger)
//}
//
//func NewAnnounceLoginConsumerServiceForFx(configs *session_config.Config, logger logger.ILogger) kafka_broker.IConsumerService {
//	return session_consumer.NewAnnounceLoginKafkaConsumerService(configs, logger)
//}
