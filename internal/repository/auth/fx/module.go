package auth_repostiory_fx

import (
	"go.uber.org/fx"

	auth_repostiory "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/auth"
)

var DatabaseModule = fx.Provide(auth_repostiory.NewDatabase)
