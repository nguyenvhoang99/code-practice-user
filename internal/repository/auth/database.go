package auth_repostiory

import (
	"context"

	"golang.org/x/crypto/bcrypt"

	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"

	postgresql_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/postgresql"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"
)

var (
	postgreSQLObserverAttributes = []attribute.KeyValue{
		attribute.String("type", "database"),
		attribute.String("database", "postgresql"),
		attribute.String("repository", "auth"),
	}
)

type Database struct {
	database *postgresql_database.Database
	observer observer.IObserver
}

func NewDatabase(database *postgresql_database.Database, observer observer.IObserver) IDatabase {
	return &Database{
		database: database,
		observer: observer,
	}
}

func (d *Database) VerifyEmailAndPassword(ctx context.Context, email, password string) (bool, error) {
	defaultObserver := d.observer.Start(ctx, "verify-email-and-password", postgreSQLObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	entities := make([]*AuthByEmailEntity, 0)

	err := d.database.Client.SelectContext(observerCtx,
		&entities, "SELECT password FROM auth_by_email WHERE email = $1", email)
	if err != nil {
		d.observer.LogError(observerCtx, "error when find auth by email", zap.Any("email", email),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return false, err
	}

	if len(entities) == 0 {
		return false, nil
	}

	return d.comparePassword(password, entities[0].Password), nil
}

func (d *Database) comparePassword(inputPassword, databasePassword string) bool {
	return bcrypt.CompareHashAndPassword([]byte(databasePassword), []byte(inputPassword)) == nil
}
