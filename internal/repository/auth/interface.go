package auth_repostiory

import "context"

type ICache interface {
	VerifyPhoneAndOneTimePassword(ctx context.Context, email, oneTimePassword string) (bool, error)
}

type IDatabase interface {
	VerifyEmailAndPassword(ctx context.Context, email, password string) (bool, error)
}
