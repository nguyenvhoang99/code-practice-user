package group_repository_fx

import (
	"go.uber.org/fx"

	group_repostiory "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/group"
)

var DatabaseModule = fx.Provide(group_repostiory.NewDatabase)
