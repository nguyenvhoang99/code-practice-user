package group_repository

type GroupEntity struct {
	GroupId string `json:"group_id,omitempty" bson:"_id,omitempty"`
	Type    string `json:"type,omitempty" bson:"type,omitempty"`
	Status  bool   `json:"status,omitempty" bson:"status,omitempty"`
	Name    string `json:"name,omitempty" bson:"name,omitempty"`
	Avatar  string `json:"avatar,omitempty" bson:"avatar,omitempty"`

	CreatedBy string `json:"created_by,omitempty" bson:"created_by,omitempty"`
	UpdatedBy string `json:"updated_by,omitempty" bson:"updated_by,omitempty"`
	CreatedAt int64  `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt int64  `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

type GroupUserEntity struct {
	GroupId string `db:"group_id"`
	UserId  string `db:"user_id"`
	Role    string `db:"role"`
}
