package group_repository

import (
	"context"
	"errors"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"

	mongodb_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/mongodb"
	postgresql_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/postgresql"
	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

var (
	ErrInactiveGroup = &custom_err.InvalidErr{Err: "inactive group"}

	mongoDBObserverAttributes = []attribute.KeyValue{
		attribute.String("type", "database"),
		attribute.String("database", "mongodb"),
		attribute.String("repository", "group"),
	}
	postgreSQLObserverAttributes = []attribute.KeyValue{
		attribute.String("type", "database"),
		attribute.String("database", "postgresql"),
		attribute.String("repository", "group"),
	}
)

type database struct {
	mongoDBDatabase    *mongodb_database.Database
	postgreSQLDatabase *postgresql_database.Database
	observer           observer.IObserver
}

func NewDatabase(mongoDBDatabase *mongodb_database.Database, postgreSQLDatabase *postgresql_database.Database,
	observer observer.IObserver) IDatabase {
	return &database{
		mongoDBDatabase:    mongoDBDatabase,
		postgreSQLDatabase: postgreSQLDatabase,
		observer:           observer,
	}
}

func (d *database) InsertGroup(ctx context.Context, group *model.Group, createdBy string) (string, error) {
	defaultObserver := d.observer.Start(ctx, "insert-group", mongoDBObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	entity := fromGroupAndCreatedByToInsertEntity(group, createdBy) // Can use entity when inserting, so model -> entity
	entity.Status = true

	result, err := d.mongoDBDatabase.Client.Collection("group").InsertOne(observerCtx, entity)
	if err != nil {
		d.observer.LogError(observerCtx, "error when insert group", zap.Any("entity", entity),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return "", err
	}

	groupIdHex, ok := result.InsertedID.(primitive.ObjectID)
	if !ok {
		return result.InsertedID.(string), nil
	}
	return groupIdHex.Hex(), nil
}

func (d *database) UpdateGroup(ctx context.Context, group *model.Group, updatedBy string) (bool, error) {
	defaultObserver := d.observer.Start(ctx, "update-group", mongoDBObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	filter := bson.M{"_id": group.GroupId}
	upsert := false

	entity := fromGroupAndUpdatedByToMongoDBUpdateEntity(group, updatedBy) // Only use bson.M when updating, so model -> bson.M

	result, err := d.mongoDBDatabase.Client.Collection("group").UpdateOne(observerCtx, filter, bson.M{"$set": entity},
		&options.UpdateOptions{
			Upsert: &upsert,
		})
	if err != nil {
		d.observer.LogError(observerCtx, "error when update group", zap.Any("filter", filter), zap.Any("entity", entity),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return false, err
	}
	if result.MatchedCount == 0 || result.ModifiedCount != 1 {
		return false, nil
	}

	return true, nil
}

func (d *database) FindGroup(ctx context.Context, groupId string) (*model.Group, error) {
	defaultObserver := d.observer.Start(ctx, "find-group", mongoDBObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	filter := bson.M{"_id": groupId}
	projection := bson.M{
		"type":   1,
		"status": 1,
		"name":   1,
		"avatar": 1,
	}

	entity := &GroupEntity{} // Can use entity when finding, so model -> entity (but later in return)

	err := d.mongoDBDatabase.Client.Collection("group").FindOne(observerCtx, filter,
		&options.FindOneOptions{
			Projection: projection,
		}).Decode(entity)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, nil
		}
		d.observer.LogError(observerCtx, "error when find group", zap.Any("filter", filter), zap.Any("projection", projection),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	if !entity.Status {
		return nil, ErrInactiveGroup
	}

	return fromEntityToGroup(entity), nil
}

func (d *database) AddUserToGroup(ctx context.Context, groupId, userId, role string) (bool, error) {
	defaultObserver := d.observer.Start(ctx, "add-user-to-group", postgreSQLObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	res, err := d.postgreSQLDatabase.Client.ExecContext(observerCtx,
		"INSERT INTO group_users (group_id, user_id, role) VALUES ($1, $2, $3)", groupId, userId, role)
	if err != nil {
		d.observer.LogError(observerCtx, "error when add user to group", zap.String("groupId", groupId), zap.String("userId", userId),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return false, err
	}

	rowsAffected, _ := res.RowsAffected()
	return rowsAffected == 1, err
}

func (d *database) UpdateUserInGroup(ctx context.Context, groupId, userId, role string) (bool, error) {
	defaultObserver := d.observer.Start(ctx, "add-user-to-group", postgreSQLObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	res, err := d.postgreSQLDatabase.Client.ExecContext(observerCtx,
		"UPDATE group_users SET role = $3 WHERE group_id = $1 AND user_id = $2", groupId, userId, role)
	if err != nil {
		d.observer.LogError(observerCtx, "error when update user in group", zap.String("groupId", groupId), zap.String("userId", userId),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return false, err
	}

	rowsAffected, _ := res.RowsAffected()
	return rowsAffected == 1, err
}

func (d *database) RemoveUserFromGroup(ctx context.Context, groupId, userId string) (bool, error) {
	defaultObserver := d.observer.Start(ctx, "remove-user-from-group", postgreSQLObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	res, err := d.postgreSQLDatabase.Client.ExecContext(observerCtx,
		"DELETE FROM group_users WHERE group_id = $1 AND user_id = $2", groupId, userId)
	if err != nil {
		d.observer.LogError(observerCtx, "error when remove user from group", zap.String("groupId", groupId), zap.String("userId", userId),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return false, err
	}

	rowsAffected, _ := res.RowsAffected()
	return rowsAffected == 1, err
}

func (d *database) QueryUserInGroup(ctx context.Context, groupId string, userId string) (string, error) {
	defaultObserver := d.observer.Start(ctx, "query-user-in-group", postgreSQLObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	entities := make([]*GroupUserEntity, 0)

	err := d.postgreSQLDatabase.Client.SelectContext(observerCtx,
		&entities, "SELECT role FROM group_users WHERE group_id = $1 AND user_id = $2", groupId, userId)
	if err != nil {
		d.observer.LogError(observerCtx, "error when query user in group", zap.String("groupId", groupId), zap.String("userId", userId),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return "", err
	}

	if len(entities) == 0 {
		return "", nil
	}
	return entities[0].Role, nil
}

func (d *database) QueryUsersInGroup(ctx context.Context, groupId string) (map[string]string, error) {
	defaultObserver := d.observer.Start(ctx, "query-users-in-group", postgreSQLObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	entities := make([]*GroupUserEntity, 0)

	err := d.postgreSQLDatabase.Client.SelectContext(observerCtx,
		&entities, "SELECT user_id, role FROM group_users WHERE group_id = $1", groupId)
	if err != nil {
		d.observer.LogError(observerCtx, "error when query users in group", zap.String("groupId", groupId),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	users := make(map[string]string)
	for _, entity := range entities {
		users[entity.UserId] = entity.Role
	}
	return users, err
}

func (d *database) QueryGroupsOfUser(ctx context.Context, userId string) (map[string]string, error) {
	defaultObserver := d.observer.Start(ctx, "query-groups-of-user", postgreSQLObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	entities := make([]*GroupUserEntity, 0)

	err := d.postgreSQLDatabase.Client.SelectContext(observerCtx,
		&entities, "SELECT group_id, role FROM group_users WHERE user_id = $1", userId)
	if err != nil {
		d.observer.LogError(observerCtx, "error when query groups of user", zap.String("userId", userId),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	groups := make(map[string]string)
	for _, entity := range entities {
		groups[entity.GroupId] = entity.Role
	}
	return groups, err
}

func fromGroupAndCreatedByToInsertEntity(group *model.Group, createdBy string) *GroupEntity {
	return &GroupEntity{
		Type:      group.Type,
		Name:      group.Name,
		Avatar:    group.Avatar,
		CreatedBy: createdBy,
		CreatedAt: time.Now().Unix(),
	}
}

func fromGroupAndUpdatedByToMongoDBUpdateEntity(group *model.Group, updatedBy string) bson.M {
	return bson.M{
		"type":       group.Type,
		"name":       group.Name,
		"avatar":     group.Avatar,
		"updated_by": updatedBy,
		"updated_at": time.Now().Unix(),
	}
}

func fromEntityToGroup(entity *GroupEntity) *model.Group {
	return &model.Group{
		GroupId: entity.GroupId,
		Type:    entity.Type,
		Name:    entity.Name,
		Avatar:  entity.Avatar,
	}
}
