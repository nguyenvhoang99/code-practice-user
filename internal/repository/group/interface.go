package group_repository

import (
	"context"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

type IDatabase interface {
	InsertGroup(ctx context.Context, group *model.Group, createdBy string) (string, error)
	UpdateGroup(ctx context.Context, group *model.Group, updatedBy string) (bool, error)
	FindGroup(ctx context.Context, groupId string) (*model.Group, error)

	AddUserToGroup(ctx context.Context, groupId, userId, role string) (bool, error)
	UpdateUserInGroup(ctx context.Context, groupId, userId, role string) (bool, error)
	RemoveUserFromGroup(ctx context.Context, groupId, userId string) (bool, error)
	QueryUserInGroup(ctx context.Context, groupId, userId string) (string, error)
	QueryUsersInGroup(ctx context.Context, groupId string) (map[string]string, error) // key: userId, value: role
	QueryGroupsOfUser(ctx context.Context, userId string) (map[string]string, error)  // key: groupId, value: role
}
