package session_repository

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/redis/go-redis/v9"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-pkg/pkg/app"
	redis_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/redis"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/config/session"
)

var baseObserverAttributes = []attribute.KeyValue{
	attribute.String("database", "redis"),
	attribute.String("repository", "auth"),
}

type Cache struct {
	keyPrefix string
	config    *session_config.Config
	database  *redis_database.Database
	observer  observer.IObserver
}

func NewCache(appConfig *app.Config, config *session_config.Config, database *redis_database.Database,
	observer observer.IObserver) ICache {
	return &Cache{
		keyPrefix: fmt.Sprintf("%s.SESSION", strings.ToUpper(appConfig.GetServiceName())),
		config:    config,
		database:  database,
		observer:  observer,
	}
}

func (r *Cache) GetSession(ctx context.Context, userId, platform string) (*SessionEntity, error) {
	defaultObserver := r.observer.Start(ctx, "get-auth", getObserverAttributes(userId, platform)...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	result := r.database.Client.HGet(observerCtx, r.sessionsKey(userId), platform)
	if err := result.Err(); err != nil {
		if errors.Is(err, redis.Nil) {
			return nil, nil
		}
		r.observer.LogError(observerCtx, "error when get rSession", zap.Any("args", result.Args()),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	entity := &SessionEntity{}

	err := json.Unmarshal([]byte(result.Val()), entity)
	if err != nil {
		r.observer.LogError(observerCtx, "error when deserialize from rSession", zap.Any("args", result.Args()),
			zap.Any("value", result.Val()), zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	return entity, nil
}

func (r *Cache) PutSession(ctx context.Context, userId, platform string, entity *SessionEntity) error {
	defaultObserver := r.observer.Start(ctx, "put-auth", getObserverAttributes(userId, platform)...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	rEntity, err := json.Marshal(entity)
	if err != nil {
		r.observer.LogError(observerCtx, "error when serialize to rSession", zap.String("userId", userId), zap.String("platform", platform),
			zap.Any("entity", entity), zap.Error(err))
		defaultObserver.WithErr(err)
		return err
	}

	var result *redis.IntCmd
	_, err = r.database.Client.Pipelined(observerCtx, func(p redis.Pipeliner) error {
		result = p.HSet(observerCtx, r.sessionsKey(userId), platform, string(rEntity))
		p.Expire(observerCtx, r.sessionsKey(userId), time.Duration(r.config.CacheTTLInHour)*time.Hour)
		return nil
	})
	if err != nil {
		r.observer.LogError(observerCtx, "error when execute pipeline to put rSession", zap.Any("args", result.Args()),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return err
	}

	return nil
}

func (r *Cache) DelSession(ctx context.Context, userId, platform string) error {
	defaultObserver := r.observer.Start(ctx, "del-auth", getObserverAttributes(userId, platform)...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	var result *redis.IntCmd
	_, err := r.database.Client.Pipelined(observerCtx, func(p redis.Pipeliner) error {
		result = p.HDel(observerCtx, r.sessionsKey(userId), platform)
		p.Expire(observerCtx, r.sessionsKey(userId), time.Duration(r.config.CacheTTLInHour)*time.Hour)
		return nil
	})
	if err != nil {
		r.observer.LogError(observerCtx, "error when execute pipeline to del rSession", zap.Any("args", result.Args()),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return err
	}

	return nil
}

func (r *Cache) sessionsKey(userId string) string {
	return fmt.Sprintf("%s.%s", r.keyPrefix, userId)
}

func getObserverAttributes(userId, platform string) []attribute.KeyValue {
	observerAttributes := make([]attribute.KeyValue, 0)
	observerAttributes = append(observerAttributes, baseObserverAttributes...)
	observerAttributes = append(observerAttributes, attribute.String("userId", userId))
	observerAttributes = append(observerAttributes, attribute.String("platform", platform))
	return observerAttributes
}
