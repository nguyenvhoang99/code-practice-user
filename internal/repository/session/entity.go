package session_repository

type SessionEntity struct {
	SessionId    string              `json:"session_id"`
	LoginSession *LoginSessionEntity `json:"login_session"`
	CreatedAt    int64               `json:"created_at"`
	ExpiredAt    int64               `json:"expired_at"`
}

type LoginSessionEntity struct {
	UserIP    string `json:"user_ip,omitempty"`
	UserAgent string `json:"user_agent,omitempty"`
	Platform  string `json:"platform,omitempty"`
	Version   string `json:"version,omitempty"`
	Device    string `json:"device,omitempty"`
}
