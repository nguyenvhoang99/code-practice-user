package session_repository

import "context"

type ICache interface {
	GetSession(ctx context.Context, userId, platform string) (*SessionEntity, error)
	PutSession(ctx context.Context, userId, platform string, sessionEntity *SessionEntity) error
	DelSession(ctx context.Context, userId, platform string) error
}
