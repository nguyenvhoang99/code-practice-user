package session_repository_fx

import (
	"go.uber.org/fx"

	session_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/session"
)

var CacheModule = fx.Provide(session_repository.NewCache)
