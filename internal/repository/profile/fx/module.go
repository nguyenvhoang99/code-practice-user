package profile_repository_fx

import (
	"go.uber.org/fx"

	profile_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/profile"
)

var CacheModule = fx.Provide(profile_repository.NewCache)
var DatabaseModule = fx.Provide(profile_repository.NewDatabase)
