package profile_repository

import (
	"context"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

type ICache interface {
	GetProfileIdsByUserId(ctx context.Context, userId string) (*model.ProfileIds, error)
	SetProfileIdsByUserId(ctx context.Context, userId string, profileIds *model.ProfileIds) error

	GetProfileIdsByEmail(ctx context.Context, email string) (*model.ProfileIds, error)
	SetProfileIdsByEmail(ctx context.Context, email string, profileIds *model.ProfileIds) error

	GetProfileIdsByPhone(ctx context.Context, phone string) (*model.ProfileIds, error)
	SetProfileIdsByPhone(ctx context.Context, phone string, profileIds *model.ProfileIds) error

	GetBasicProfile(ctx context.Context, userId string) (*model.BasicProfile, error)
	SetBasicProfile(ctx context.Context, userId string, basicProfile *model.BasicProfile) error

	GetBindingProfiles(ctx context.Context, userId string, all bool, appNames []string) (map[string]*model.BindingProfile, error)
	SetBindingProfiles(ctx context.Context, userId string, bindingProfiles map[string]*model.BindingProfile) error
}

type IDatabase interface {
	InsertProfile(ctx context.Context, profile *model.Profile) (string, error)
	UpdateProfile(ctx context.Context, profileIds *model.ProfileIds, profile *model.Profile) (bool, error)
	FindProfileById(ctx context.Context, profileIds *model.ProfileIds, queryOption *model.QueryProfileOption) (*model.Profile, error)
	FindProfileByBindingId(ctx context.Context, profileBindingId *model.ProfileBindingIds, queryOption *model.QueryProfileOption) (*model.Profile, error)
}
