package profile_repository

type QueryProfileEntityOption struct {
	Ids                    *ProfileEntityIds // Priorities: 1st UserId, 2nd Email, 3rd Phone
	WithBasicProfile       bool
	WithAllBindingProfiles bool
	BindingAppNames        []string
}

type ProfileEntityIds struct {
	UserId string
	Email  string
	Phone  string
}

type ProfileEntity struct {
	UserId          string                           `json:"user_id,omitempty" bson:"_id,omitempty"`
	Email           string                           `json:"email,omitempty" bson:"email,omitempty"`
	Phone           string                           `json:"phone,omitempty" bson:"phone,omitempty"`
	Status          bool                             `json:"status,omitempty" bson:"status,omitempty"`
	BasicProfile    *BasicProfileEntity              `json:"basic_profile,omitempty" bson:"basic_profile,omitempty"`
	BindingProfiles map[string]*BindingProfileEntity `json:"binding_profiles,omitempty" bson:"binding_profiles,omitempty"`
}

type BasicProfileEntity struct {
	Name   string `json:"name,omitempty" bson:"name,omitempty"`
	Avatar string `json:"avatar,omitempty" bson:"avatar,omitempty"`
}

type BindingProfileEntity struct {
	AppName   string `json:"app_name,omitempty" bson:"app_name,omitempty"`
	AppUserId string `json:"app_user_id,omitempty" bson:"app_user_id,omitempty"`
	AppEmail  string `json:"app_email,omitempty" bson:"app_email,omitempty"`
	AppPhone  string `json:"app_phone,omitempty" bson:"app_phone,omitempty"`
}
