package profile_repository

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/redis/go-redis/v9"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-pkg/pkg/app"
	redis_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/redis"
	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	profile_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/config/profile"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

const (
	mockNilForSliceOrMap = "mock_nil_key"
)

var (
	ErrInvalidRedisArg = &custom_err.InvalidErr{Err: "invalid redis' profile repository arg"}

	redisObserverAttributes = []attribute.KeyValue{
		attribute.String("type", "database"),
		attribute.String("database", "redis"),
		attribute.String("repository", "profile"),
	}
)

type Cache struct {
	keyPrefix string
	config    *profile_config.Config
	database  *redis_database.Database
	observer  observer.IObserver
}

func NewCache(appConfig *app.Config, config *profile_config.Config, database *redis_database.Database,
	observer observer.IObserver) ICache {
	return &Cache{
		keyPrefix: fmt.Sprintf("%s.PROFILE", strings.ToUpper(appConfig.GetServiceName())),
		config:    config,
		database:  database,
		observer:  observer,
	}
}

func (c *Cache) GetProfileIdsByUserId(ctx context.Context, userId string) (*model.ProfileIds, error) {
	defaultObserver := c.observer.Start(ctx, "get-profile-ids-by-user-id", redisObserverAttributes...)
	defer defaultObserver.End()

	return c.baseHandleGetProfileIds(defaultObserver, c.profileIdsByUserIdKey(userId))
}

func (c *Cache) SetProfileIdsByUserId(ctx context.Context, userId string, profileIds *model.ProfileIds) error {
	defaultObserver := c.observer.Start(ctx, "set-profile-ids-by-user-id", redisObserverAttributes...)
	defer defaultObserver.End()

	return c.baseHandleSetProfileIds(defaultObserver, c.profileIdsByUserIdKey(userId), profileIds)
}

func (c *Cache) GetProfileIdsByEmail(ctx context.Context, email string) (*model.ProfileIds, error) {
	defaultObserver := c.observer.Start(ctx, "get-profile-ids-by-email", redisObserverAttributes...)
	defer defaultObserver.End()

	return c.baseHandleGetProfileIds(defaultObserver, c.profileIdsByEmailKey(email))
}

func (c *Cache) SetProfileIdsByEmail(ctx context.Context, email string, profileIds *model.ProfileIds) error {
	defaultObserver := c.observer.Start(ctx, "set-profile-ids-by-email", redisObserverAttributes...)
	defer defaultObserver.End()

	return c.baseHandleSetProfileIds(defaultObserver, c.profileIdsByEmailKey(email), profileIds)
}

func (c *Cache) GetProfileIdsByPhone(ctx context.Context, phone string) (*model.ProfileIds, error) {
	defaultObserver := c.observer.Start(ctx, "get-profile-ids-by-phone", redisObserverAttributes...)
	defer defaultObserver.End()

	return c.baseHandleGetProfileIds(defaultObserver, c.profileIdsByPhoneKey(phone))
}

func (c *Cache) SetProfileIdsByPhone(ctx context.Context, phone string, profileIds *model.ProfileIds) error {
	defaultObserver := c.observer.Start(ctx, "set-profile-ids-by-phone", redisObserverAttributes...)
	defer defaultObserver.End()

	return c.baseHandleSetProfileIds(defaultObserver, c.profileIdsByPhoneKey(phone), profileIds)
}

func (c *Cache) baseHandleGetProfileIds(defaultObserver *observer.DefaultObserver, key string) (*model.ProfileIds, error) {
	observerCtx := defaultObserver.Ctx()

	result := c.database.Client.HGetAll(observerCtx, key)
	if err := result.Err(); err != nil {
		if errors.Is(err, redis.Nil) {
			return nil, nil
		}
		c.observer.LogError(observerCtx, "error when get rProfileIds", zap.Any("args", result.Args()),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, nil
	}
	if len(result.Val()) == 0 {
		return nil, nil
	}

	return &model.ProfileIds{
		UserId: result.Val()["user_id"],
		Email:  result.Val()["email"],
		Phone:  result.Val()["phone"],
	}, nil
}

func (c *Cache) baseHandleSetProfileIds(defaultObserver *observer.DefaultObserver, key string, profileIds *model.ProfileIds) error {
	observerCtx := defaultObserver.Ctx()

	var result *redis.IntCmd
	_, err := c.database.Client.Pipelined(observerCtx, func(p redis.Pipeliner) error {
		result = p.HSet(observerCtx, key, "user_id", profileIds.UserId, "email", profileIds.Email, "phone", profileIds.Phone)
		p.Expire(observerCtx, key, time.Duration(c.config.CacheTTLInDay)*24*time.Hour)
		return nil
	})
	if err != nil {
		c.observer.LogError(observerCtx, "error when execute pipeline to set rProfileIds", zap.Any("args", result.Args()),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return err
	}
	if result.Err() != nil {
		c.observer.LogError(observerCtx, "error when set rProfileIds", zap.Any("args", result.Args()),
			zap.Error(result.Err()))
		defaultObserver.WithErr(result.Err())
		return err
	}

	return nil
}

func (c *Cache) GetBasicProfile(ctx context.Context, userId string) (*model.BasicProfile, error) {
	defaultObserver := c.observer.Start(ctx, "get-basic-profile", redisObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	result := c.database.Client.Get(observerCtx, c.basicProfileKey(userId))
	if err := result.Err(); err != nil {
		if !errors.Is(err, redis.Nil) {
			c.observer.LogError(observerCtx, "error when get rBasicProfile", zap.Any("args", result.Args()),
				zap.Error(err))
			defaultObserver.WithErr(err)
			return nil, err
		}
		return nil, nil
	}

	basicProfile := &model.BasicProfile{}

	err := json.Unmarshal([]byte(result.Val()), basicProfile)
	if err != nil {
		c.observer.LogError(observerCtx, "error when deserialize from rBasicProfile", zap.Any("args", result.Args()),
			zap.Any("value", result.Val()), zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	return basicProfile, nil
}

func (c *Cache) SetBasicProfile(ctx context.Context, userId string, basicProfile *model.BasicProfile) error {
	defaultObserver := c.observer.Start(ctx, "set-basic-profile", redisObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	rBasicProfile, err := json.Marshal(basicProfile)
	if err != nil {
		c.observer.LogError(observerCtx, "error when serialize to rBasicProfile", zap.String("userId", userId),
			zap.Any("basicProfile", basicProfile), zap.Error(err))
		defaultObserver.WithErr(err)
		return err
	}

	result := c.database.Client.Set(observerCtx, c.basicProfileKey(userId), string(rBasicProfile),
		time.Duration(c.config.CacheTTLInDay)*24*time.Hour)
	if err = result.Err(); err != nil {
		c.observer.LogError(observerCtx, "error when set rBasicProfile", zap.Any("args", result.Args()),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return err
	}

	return nil
}

func (c *Cache) GetBindingProfiles(ctx context.Context, userId string, all bool, appNames []string) (map[string]*model.BindingProfile, error) {
	defaultObserver := c.observer.Start(ctx, "get-binding-profiles", redisObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	bindingProfiles := make(map[string]*model.BindingProfile)

	if all {
		result := c.database.Client.HGetAll(observerCtx, c.bindingProfileKey(userId))
		if err := result.Err(); err != nil {
			if errors.Is(err, redis.Nil) {
				return nil, nil
			}
			c.observer.LogError(observerCtx, "error when get all rBindingProfiles", zap.Any("args", result.Args()),
				zap.Error(err))
			defaultObserver.WithErr(err)
			return nil, err
		}
		if len(result.Val()) == 0 {
			return nil, nil
		}

		for rBindingAppName, rBindingProfile := range result.Val() {
			if rBindingAppName == mockNilForSliceOrMap {
				continue
			}

			bindingProfile := &model.BindingProfile{}
			err := json.Unmarshal([]byte(rBindingProfile), bindingProfile)
			if err != nil {
				c.observer.LogError(observerCtx, "error when serialize from rBindingProfile", zap.String("userId", userId),
					zap.Any("rBindingProfile", rBindingProfile), zap.Error(err))
				return nil, err
			}
			bindingProfiles[bindingProfile.AppName] = bindingProfile
		}

		return bindingProfiles, nil
	}

	if len(appNames) > 0 {
		result := c.database.Client.HMGet(observerCtx, c.bindingProfileKey(userId), appNames...)
		if err := result.Err(); err != nil {
			if errors.Is(err, redis.Nil) {
				return nil, nil
			}
			c.observer.LogError(observerCtx, "error when get some rBindingProfiles", zap.Any("args", result.Args()),
				zap.Error(err))
			defaultObserver.WithErr(err)
			return nil, err
		}
		if len(result.Val()) == 0 {
			return nil, nil
		}

		for _, iBindingProfile := range result.Val() {
			if iBindingProfile == nil {
				continue
			}

			rBindingProfile, ok := iBindingProfile.(string)
			if !ok || rBindingProfile == mockNilForSliceOrMap {
				continue
			}

			bindingProfile := &model.BindingProfile{}
			err := json.Unmarshal([]byte(rBindingProfile), bindingProfile)
			if err != nil {
				c.observer.LogError(observerCtx, "error when serialize from rBindingProfile", zap.String("userId", userId),
					zap.Any("rBindingProfile", rBindingProfile), zap.Error(err))
				return nil, err
			}
			bindingProfiles[bindingProfile.AppName] = bindingProfile
		}

		return bindingProfiles, nil
	}

	return nil, ErrInvalidRedisArg
}

func (c *Cache) SetBindingProfiles(ctx context.Context, userId string, bindingProfiles map[string]*model.BindingProfile) error {
	defaultObserver := c.observer.Start(ctx, "set-binding-profile", redisObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	rBindingProfiles := make([]interface{}, 0)
	for _, bindingProfile := range bindingProfiles {
		rBindingProfile, err := json.Marshal(bindingProfile)
		if err != nil {
			c.observer.LogError(observerCtx, "error when serialize to rBindingProfile", zap.String("userId", userId),
				zap.Any("bindingProfile", bindingProfile), zap.Error(err))
			defaultObserver.WithErr(err)
			return err
		}

		rBindingProfiles = append(rBindingProfiles, bindingProfile.AppName, string(rBindingProfile))
	}
	if len(rBindingProfiles) == 0 {
		rBindingProfiles = append(rBindingProfiles, mockNilForSliceOrMap, mockNilForSliceOrMap)
	}

	var result *redis.BoolCmd
	_, err := c.database.Client.Pipelined(observerCtx, func(p redis.Pipeliner) error {
		result = c.database.Client.HMSet(observerCtx, c.bindingProfileKey(userId), rBindingProfiles...)
		p.Expire(observerCtx, c.bindingProfileKey(userId), time.Duration(c.config.CacheTTLInDay)*24*time.Hour)
		return nil
	})
	if err != nil {
		c.observer.LogError(observerCtx, "error when execute pipeline to set some rBindingProfiles", zap.Any("args", result.Args()),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return err
	}
	if result.Err() != nil {
		c.observer.LogError(observerCtx, "error when set some rBindingProfiles", zap.Any("args", result.Args()),
			zap.Error(result.Err()))
		defaultObserver.WithErr(result.Err())
		return err
	}

	return nil
}

func (c *Cache) profileIdsByUserIdKey(userId string) string {
	return fmt.Sprintf("%s.IDS_BY_USER_ID.%s", c.keyPrefix, userId)
}

func (c *Cache) profileIdsByEmailKey(email string) string {
	return fmt.Sprintf("%s.IDS_BY_EMAIL.%s", c.keyPrefix, email)
}

func (c *Cache) profileIdsByPhoneKey(phone string) string {
	return fmt.Sprintf("%s.IDS_BY_PHONE.%s", c.keyPrefix, phone)
}

func (c *Cache) basicProfileKey(userId string) string {
	return fmt.Sprintf("%s.BASIC.%s", c.keyPrefix, userId)
}

func (c *Cache) bindingProfileKey(userId string) string {
	return fmt.Sprintf("%s.BINDING.%s", c.keyPrefix, userId)
}
