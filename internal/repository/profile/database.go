package profile_repository

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"

	mongodb_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/mongodb"
	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

var (
	ErrInactiveUser = &custom_err.InvalidErr{Err: "inactive user"}

	mongoDBObserverAttributes = []attribute.KeyValue{
		attribute.String("type", "database"),
		attribute.String("database", "mongodb"),
		attribute.String("repository", "profile"),
	}
)

type Database struct {
	database *mongodb_database.Database
	observer observer.IObserver
}

func NewDatabase(database *mongodb_database.Database, observer observer.IObserver) IDatabase {
	return &Database{
		database: database,
		observer: observer,
	}
}

func (d *Database) InsertProfile(ctx context.Context, profile *model.Profile) (string, error) {
	defaultObserver := d.observer.Start(ctx, "insert-profile", mongoDBObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	entity := fromProfileToEntity(profile) // Can use entity when inserting, so model -> entity
	entity.Status = true

	result, err := d.database.Client.Collection("profile").InsertOne(observerCtx, entity)
	if err != nil {
		d.observer.LogError(observerCtx, "error when insert profile", zap.Any("entity", entity),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return "", err
	}

	userIdHex, ok := result.InsertedID.(primitive.ObjectID)
	if !ok {
		return result.InsertedID.(string), nil
	}
	return userIdHex.Hex(), nil
}

func (d *Database) UpdateProfile(ctx context.Context, profileIds *model.ProfileIds, profile *model.Profile) (bool, error) {
	defaultObserver := d.observer.Start(ctx, "update-profile", mongoDBObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	filter := fromIdsToFilter(profileIds)
	upsert := true

	entity := fromProfileToMongoDBEntity(profile) // Only use bson.M when updating, so model -> bson.M

	result, err := d.database.Client.Collection("profile").UpdateOne(observerCtx, filter, bson.M{"$set": entity},
		&options.UpdateOptions{
			Upsert: &upsert,
		})
	if err != nil {
		d.observer.LogError(observerCtx, "error when update profile", zap.Any("filter", filter), zap.Any("entity", entity),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return false, err
	}
	if result.MatchedCount == 0 ||
		(result.ModifiedCount != 1 && result.UpsertedCount != 1) {
		return false, nil
	}

	return true, nil
}

func (d *Database) FindProfileById(ctx context.Context, profileIds *model.ProfileIds, queryOption *model.QueryProfileOption) (*model.Profile, error) {
	defaultObserver := d.observer.Start(ctx, "find-profile-by-id", mongoDBObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	filter := fromIdsToFilter(profileIds)
	projection := fromQueryOptionToProjection(queryOption)

	entity := &ProfileEntity{} // Can use entity when finding, so model -> entity (but later in return)

	err := d.database.Client.Collection("profile").FindOne(observerCtx, filter,
		&options.FindOneOptions{
			Projection: projection,
		}).Decode(entity)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, nil
		}
		d.observer.LogError(observerCtx, "error when find profile by id", zap.Any("filter", filter), zap.Any("projection", projection),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	if !entity.Status {
		return nil, ErrInactiveUser
	}

	return fromEntityToProfile(entity), nil
}

func (d *Database) FindProfileByBindingId(ctx context.Context, profileBindingIds *model.ProfileBindingIds, queryOption *model.QueryProfileOption) (*model.Profile, error) {
	defaultObserver := d.observer.Start(ctx, "find-profile-by-binding-id", mongoDBObserverAttributes...)
	defer defaultObserver.End()

	observerCtx := defaultObserver.Ctx()

	filter := fromBindingIdsToFilter(profileBindingIds)
	projection := fromQueryOptionToProjection(queryOption)

	entity := &ProfileEntity{} // Can use entity when finding, so model -> entity (but later in return)

	err := d.database.Client.Collection("profile").FindOne(observerCtx, filter,
		&options.FindOneOptions{
			Projection: projection,
		}).Decode(entity)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, nil
		}
		d.observer.LogError(observerCtx, "error when find profile by bindingId", zap.Any("filter", filter), zap.Any("projection", projection),
			zap.Error(err))
		defaultObserver.WithErr(err)
		return nil, err
	}

	if !entity.Status {
		return nil, ErrInactiveUser
	}

	return fromEntityToProfile(entity), nil
}

func fromProfileToEntity(profile *model.Profile) *ProfileEntity {
	entity := &ProfileEntity{}
	if profile.Email != "" {
		entity.Email = profile.Email
	}
	if profile.Phone != "" {
		entity.Phone = profile.Phone
	}
	if profile.BasicProfile != nil {
		entity.BasicProfile = &BasicProfileEntity{
			Name:   profile.BasicProfile.Name,
			Avatar: profile.BasicProfile.Avatar,
		}
	}
	if profile.BindingProfiles != nil {
		entity.BindingProfiles = make(map[string]*BindingProfileEntity)
		for bindingAppName, bindingProfile := range profile.BindingProfiles {
			entity.BindingProfiles[bindingAppName] = &BindingProfileEntity{
				AppName:   bindingAppName,
				AppUserId: bindingProfile.AppUserId,
				AppEmail:  bindingProfile.AppEmail,
				AppPhone:  bindingProfile.AppPhone,
			}
		}
	}
	return entity
}

func fromProfileToMongoDBEntity(profile *model.Profile) bson.M {
	entity := bson.M{}
	if profile.Email != "" {
		entity["email"] = profile.Email
	}
	if profile.Phone != "" {
		entity["phone"] = profile.Phone
	}
	if profile.BasicProfile != nil {
		entity["basic_profile"] = &BasicProfileEntity{
			Name:   profile.BasicProfile.Name,
			Avatar: profile.BasicProfile.Avatar,
		}
	}
	for bindingAppName, bindingProfile := range profile.BindingProfiles {
		entity["binding_profiles."+bindingAppName] = &BindingProfileEntity{
			AppName:   bindingAppName,
			AppUserId: bindingProfile.AppUserId,
			AppEmail:  bindingProfile.AppEmail,
			AppPhone:  bindingProfile.AppPhone,
		}
	}
	return entity
}

func fromEntityToProfile(entity *ProfileEntity) *model.Profile {
	profile := &model.Profile{
		UserId: entity.UserId,
		Email:  entity.Email,
		Phone:  entity.Phone,
	}
	if entity.BasicProfile != nil {
		profile.BasicProfile = &model.BasicProfile{
			Name:   entity.BasicProfile.Name,
			Avatar: entity.BasicProfile.Avatar,
		}
	}
	if entity.BindingProfiles != nil {
		profile.BindingProfiles = make(map[string]*model.BindingProfile)
		for bindingAppName, bindingProfile := range entity.BindingProfiles {
			profile.BindingProfiles[bindingAppName] = &model.BindingProfile{
				AppName:   bindingAppName,
				AppUserId: bindingProfile.AppUserId,
				AppEmail:  bindingProfile.AppEmail,
				AppPhone:  bindingProfile.AppPhone,
			}
		}
	}
	return profile
}

func fromIdsToFilter(profileIds *model.ProfileIds) bson.M {
	filter := bson.M{}
	if profileIds.UserId != "" {
		userIdHex, err := primitive.ObjectIDFromHex(profileIds.UserId)
		if err != nil {
			filter["_id"] = profileIds.UserId
		} else {
			filter["_id"] = userIdHex
		}
	} else if profileIds.Email != "" {
		filter["email"] = profileIds.Email
	} else if profileIds.Phone != "" {
		filter["phone"] = profileIds.Phone
	}
	return filter
}

func fromBindingIdsToFilter(profileBindingIds *model.ProfileBindingIds) bson.M {
	filter := bson.M{}
	if profileBindingIds.Email != "" {
		filter["binding_profiles."+profileBindingIds.AppName+".email"] = profileBindingIds.Email
	} else if profileBindingIds.Phone != "" {
		filter["binding_profiles."+profileBindingIds.AppName+".phone"] = profileBindingIds.Phone
	}
	return filter
}

func fromQueryOptionToProjection(queryOption *model.QueryProfileOption) bson.M {
	projection := bson.M{
		"_id":    1,
		"email":  1,
		"phone":  1,
		"status": 1,
	}
	if queryOption.WithBasicProfile {
		projection["basic_profile"] = 1
	}
	if queryOption.WithAllBindingProfiles {
		projection["binding_profiles"] = 1
	} else {
		for _, bindingAppName := range queryOption.BindingAppNames {
			projection["binding_profiles."+bindingAppName] = 1
		}
	}
	return projection
}
