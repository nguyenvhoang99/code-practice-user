package session_producer

//import (
//	"context"
//	"encoding/json"
//	"time"
//
//	"github.com/twmb/franz-go/pkg/kgo"
//	"go.opentelemetry.io/otel/attribute"
//	"go.uber.org/zap"
//
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
//	kafka_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/kafka"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
//	tracer_utils "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer/utils"
//
//	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/configs/auth"
//)
//
//type Kafka struct {
//	configs    *session_config.Config
//	appConfig *app.Config
//	broker    *kafka_broker.Broker
//	logger    logger.ILogger
//	recorder  recorder.IRecorder
//	tracer    tracer.ITracer
//}
//
//func NewKafka(configs *session_config.Config, appConfig *app.Config, broker *kafka_broker.Broker,
//	logger logger.ILogger, recorder recorder.IRecorder, tracer tracer.ITracer) IKafka {
//	return &Kafka{
//		configs:    configs,
//		appConfig: appConfig,
//		broker:    broker,
//		logger:    logger,
//		recorder:  recorder,
//		tracer:    tracer,
//	}
//}
//
//func (k *Kafka) AnnounceLogin(ctx context.Context, message *SessionMessage) error {
//	traceCtx, span := k.tracer.Trace(ctx, "announce-login", attribute.String("broker", "kafka"),
//		attribute.String("topic", k.configs.AnnounceLoginKafkaTopic), attribute.String("userId", message.UserId))
//
//	var startTime time.Time = time.Now()
//	var err error
//	defer func() {
//		go k.recorder.Record(k.appConfig.Name, "announce-login", "broker-kafka", startTime, err)
//	}()
//
//	kMessage, err := json.Marshal(message)
//	if err != nil {
//		k.logger.ErrorCtx(traceCtx, "error when serialize to kMessage", zap.String("userId", message.UserId),
//			zap.Any("message", message), zap.Error(err))
//		tracer_utils.TraceErr(traceCtx, err)
//		span.End()
//		return err
//	}
//
//	kHeaders := make([]kgo.RecordHeader, 0)
//	k.broker.Client.Produce(traceCtx,
//		&kgo.Record{
//			Topic:   k.configs.AnnounceLoginKafkaTopic,
//			Key:     []byte(message.UserId),
//			Value:   kMessage,
//			Headers: kHeaders,
//		},
//		func(record *kgo.Record, err error) {
//			defer span.End()
//			if err != nil {
//				k.logger.ErrorCtx(traceCtx, "failed to produce for announcing login", zap.String("topic", k.configs.AnnounceLoginKafkaTopic),
//					zap.String("key", message.UserId), zap.String("value", string(kMessage)), zap.Error(err))
//				tracer_utils.TraceErr(traceCtx, err)
//				return
//			}
//			k.logger.InfoCtx(traceCtx, "produced for announcing login", zap.String("topic", k.configs.AnnounceLoginKafkaTopic),
//				zap.String("key", message.UserId), zap.String("value", string(kMessage)))
//		},
//	)
//	return nil
//}
