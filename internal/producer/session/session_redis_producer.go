package session_producer

//
//import (
//	"context"
//	"encoding/json"
//	"time"
//
//	"go.opentelemetry.io/otel/attribute"
//	"go.uber.org/zap"
//
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/app"
//	redis_broker "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/broker/redis"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/logger"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/recorder"
//	"gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer"
//	tracer_utils "gitlab.com/nguyenvhoang99/common-go-pkg/pkg/tracer/utils"
//
//	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/configs/auth"
//)
//
//type Redis struct {
//	broker    *redis_broker.Broker
//	configs    *session_config.Config
//	appConfig *app.Config
//	logger    logger.ILogger
//	recorder  recorder.IRecorder
//	tracer    tracer.ITracer
//}
//
//func NewRedis(broker *redis_broker.Broker, configs *session_config.Config, appConfig *app.Config,
//	logger logger.ILogger, recorder recorder.IRecorder, tracer tracer.ITracer) IRedis {
//	return &Redis{
//		broker:    broker,
//		configs:    configs,
//		appConfig: appConfig,
//		logger:    logger,
//		recorder:  recorder,
//		tracer:    tracer,
//	}
//}
//
//func (r *Redis) OnCreateSession(ctx context.Context, message *SessionMessage) error {
//	traceCtx, span := r.tracer.Trace(ctx, "on-create-auth", attribute.String("broker", "redis"),
//		attribute.String("channel", r.configs.OnCreateSessionRedisChannel), attribute.String("userId", message.UserId))
//	defer span.End()
//
//	var startTime time.Time = time.Now()
//	var err error
//	defer func() {
//		go r.recorder.Record(r.appConfig.Name, "on-create-auth", "broker-redis", startTime, err)
//	}()
//
//	rMessage, err := json.Marshal(message)
//	if err != nil {
//		r.logger.ErrorCtx(traceCtx, "error when serialize to rMessage", zap.String("userId", message.UserId),
//			zap.Any("message", message), zap.Error(err))
//		tracer_utils.TraceErr(traceCtx, err)
//		return err
//	}
//
//	_, err = r.broker.Client.Publish(traceCtx, r.configs.OnCreateSessionRedisChannel, rMessage).Result()
//	if err != nil {
//		r.logger.ErrorCtx(traceCtx, "error when produce on create auth", zap.String("channel", r.configs.OnCreateSessionRedisChannel),
//			zap.String("message", string(rMessage)), zap.Error(err))
//		return err
//	}
//	r.logger.InfoCtx(traceCtx, "produced on create auth", zap.String("channel", r.configs.OnCreateSessionRedisChannel),
//		zap.String("message", string(rMessage)))
//	return nil
//}
