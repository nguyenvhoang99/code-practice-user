package constants

const (
	Group_RoleAdmin    = "admin"
	Group_RoleTeacher  = "teacher"
	Group_RoleLecturer = "lecturer"
	Group_RoleMember   = "member"
)

var (
	Group_AllRoles = []string{Group_RoleAdmin, Group_RoleTeacher, Group_RoleLecturer, Group_RoleMember}
)
