package group_service

import (
	"context"

	"go.uber.org/zap"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/constants"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	group_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/group"
	profile_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/profile"
)

var (
	ErrNotFoundUserToAddToGroup = &custom_err.NotFoundErr{Err: "not found user to add to group"}
)

type service struct {
	database       group_repository.IDatabase
	profileService profile_service.IService
	observer       observer.IObserver
}

func NewService(database group_repository.IDatabase, profileService profile_service.IService,
	observer observer.IObserver) IService {
	return &service{
		database:       database,
		profileService: profileService,
		observer:       observer,
	}
}

func (s *service) CreateGroup(ctx context.Context, group *model.Group, createdBy string) (string, error) {
	groupId, err := s.database.InsertGroup(ctx, group, createdBy)
	if err != nil {
		return "", err
	}

	ok, err := s.database.AddUserToGroup(ctx, groupId, createdBy, constants.Group_RoleAdmin)
	if err != nil {
		s.observer.LogError(ctx, "error when add admin in flow create group", zap.String("groupId", groupId),
			zap.String("createdBy", createdBy), zap.Error(err))
	}
	if !ok {
		s.observer.LogWarn(ctx, "failed to add admin in flow create group", zap.String("groupId", groupId),
			zap.String("createdBy", createdBy), zap.Bool("ok", ok))
	}

	return groupId, nil
}

func (s *service) AddUserToGroup(ctx context.Context, groupId string, userId string, role string) (bool, error) {
	userProfile, err := s.profileService.QueryProfileByUserId(ctx, userId, &model.QueryProfileOption{})
	if err != nil {
		return false, err
	}
	if userProfile == nil {
		return false, ErrNotFoundUserToAddToGroup
	}

	return s.database.AddUserToGroup(ctx, groupId, userId, role)
}

func (s *service) CheckUserInGroup(ctx context.Context, groupId string, userId string) (string, error) {
	return s.database.QueryUserInGroup(ctx, groupId, userId)
}

func (s *service) QueryUserProfilesInGroup(ctx context.Context, groupId string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error) {
	users, err := s.database.QueryUsersInGroup(ctx, groupId)
	if err != nil {
		return nil, err
	}

	userIds := make([]string, 0)
	for userId := range users {
		userIds = append(userIds, userId)
	}

	userProfiles, err := s.profileService.QueryProfilesByUserIds(ctx, userIds, queryOption)
	if err != nil {
		return nil, err
	}
	return userProfiles, nil
}

func (s *service) QueryGroupsOfUser(ctx context.Context, userId string) (map[string]string, error) {
	return s.database.QueryGroupsOfUser(ctx, userId)
}
