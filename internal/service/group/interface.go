package group_service

import (
	"context"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

type IService interface {
	CreateGroup(ctx context.Context, group *model.Group, createdBy string) (string, error) // creator will become admin of the group

	AddUserToGroup(ctx context.Context, groupId, userId, role string) (bool, error)
	CheckUserInGroup(ctx context.Context, groupId, userId string) (string, error)
	QueryUserProfilesInGroup(ctx context.Context, groupId string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error)
	QueryGroupsOfUser(ctx context.Context, userId string) (map[string]string, error)
}
