package group_service_fx

import (
	"go.uber.org/fx"

	group_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/group"
)

var ServiceModule = fx.Provide(group_service.NewService)
