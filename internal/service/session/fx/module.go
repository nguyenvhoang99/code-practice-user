package session_service_fx

import (
	"go.uber.org/fx"

	session_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/session"
)

var ServiceModule = fx.Provide(session_service.NewService)
