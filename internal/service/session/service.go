package session_service

import (
	"context"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"go.uber.org/zap"

	"gitlab.com/nguyenvhoang99/common-pkg/pkg/app"
	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/config/session"
	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	session_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/session"
	profile_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/profile"
)

var (
	ErrEmptyUserId    = &custom_err.InvalidErr{Err: "empty userId"}
	ErrNotFoundUserId = &custom_err.NotFoundErr{Err: "not found userId"}

	ErrInvalidLoginInfo = &custom_err.InvalidErr{Err: "invalid loginInfo"}

	ErrEmptySessionId          = &custom_err.InvalidErr{Err: "empty sessionId"}
	ErrInvalidSessionId        = &custom_err.InvalidErr{Err: "invalid sessionId"}
	ErrNotFoundSessionId       = &custom_err.NotFoundErr{Err: "not found sessionId"}
	ErrAlreadyExpiredSessionId = &custom_err.FailedPreconditionErr{Err: "already expired sessionId"}
)

type Service struct {
	appConfig      *app.Config
	config         *session_config.Config
	cache          session_repository.ICache
	profileService profile_service.IService
	observer       observer.IObserver
}

func NewService(appConfig *app.Config, config *session_config.Config, cache session_repository.ICache,
	profileService profile_service.IService, observer observer.IObserver) (IService, error) {
	return &Service{
		appConfig:      appConfig,
		config:         config,
		cache:          cache,
		profileService: profileService,
		observer:       observer,
	}, nil
}

func (s *Service) CreateSession(ctx context.Context, userId string, loginInfo *model.LoginInfo) (string, error) {
	if userId == "" {
		return "", ErrEmptyUserId
	}
	if loginInfo == nil {
		return "", ErrInvalidLoginInfo
	}

	profile, err := s.profileService.QueryProfileByUserId(ctx, userId,
		&model.QueryProfileOption{
			WithBasicProfile: true,
		})
	if err != nil {
		return "", err
	}
	if profile == nil {
		return "", ErrNotFoundUserId
	}

	now := time.Now().Unix()
	session := &model.Session{
		UserId:    userId,
		LoginInfo: loginInfo,
		CreatedAt: now,
		ExpiredAt: now + int64(s.config.AliveInHour)*3600,
	}

	sessionSignature, err := session.BuildSignature()
	if err != nil {
		s.observer.LogError(ctx, "invalid auth to build sessionSignature", zap.String("userId", userId),
			zap.Any("auth", session), zap.Error(err))
		return "", err
	}
	session.Signature = sessionSignature

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, s.buildTokenClaims(session))

	sessionId, err := token.SignedString(s.config.SignKeyByteArr)
	if err != nil {
		s.observer.LogError(ctx, "invalid auth to build sessionId", zap.String("userId", userId),
			zap.Any("auth", session), zap.Error(err))
		return "", err
	}

	sessionEntity := &session_repository.SessionEntity{
		SessionId: sessionId,
		LoginSession: &session_repository.LoginSessionEntity{
			UserIP:    session.LoginInfo.UserIP,
			UserAgent: session.LoginInfo.UserAgent,
			Platform:  session.LoginInfo.Platform,
			Version:   session.LoginInfo.Version,
			Device:    session.LoginInfo.Device,
		},
		CreatedAt: session.CreatedAt,
		ExpiredAt: session.ExpiredAt,
	}

	err = s.cache.PutSession(ctx, userId, loginInfo.Platform, sessionEntity)
	if err != nil {
		return "", err
	}

	return sessionId, nil
}

func (s *Service) GetSession(ctx context.Context, sessionId string) (*model.Session, error) {
	if sessionId == "" {
		return nil, ErrEmptySessionId
	}

	tokenClaims := &model.TokenClaims{}

	token, err := jwt.ParseWithClaims(sessionId, tokenClaims,
		func(jwtToken *jwt.Token) (interface{}, error) {
			return s.config.SignKeyByteArr, nil
		},
		jwt.WithValidMethods(append(make([]string, 0), jwt.SigningMethodHS256.Name)),
		jwt.WithIssuer(s.appConfig.GetServiceName()),
		jwt.WithIssuedAt())
	if err != nil {
		s.observer.LogWarn(ctx, "invalid sessionId to parse as auth", zap.String("sessionId", sessionId),
			zap.Error(err))
		return nil, ErrInvalidSessionId
	}
	if !token.Valid {
		s.observer.LogWarn(ctx, "invalid sessionId to build as auth", zap.String("sessionId", sessionId),
			zap.Error(err))
		return nil, ErrInvalidSessionId
	}

	session := s.buildSession(tokenClaims)

	sessionSignature, err := session.BuildSignature()
	if err != nil {
		s.observer.LogWarn(ctx, "invalid auth to build sessionSignature", zap.String("sessionId", sessionId),
			zap.Any("auth", session), zap.Error(err))
		return nil, ErrInvalidSessionId
	}
	if sessionSignature != session.Signature {
		s.observer.LogWarn(ctx, "invalid auth with mismatch sessionSignature", zap.String("sessionId", sessionId),
			zap.Any("auth", session), zap.Error(err))
		return nil, ErrInvalidSessionId
	}

	sessionEntity, err := s.cache.GetSession(ctx, session.UserId, session.LoginInfo.Platform)
	if err != nil {
		return nil, err
	}
	if sessionEntity == nil || sessionEntity.SessionId != sessionId {
		return nil, ErrNotFoundSessionId
	}
	if sessionEntity.ExpiredAt < time.Now().Unix() {
		go s.delExpiredSessionInBg(ctx, session.UserId, sessionEntity.SessionId)
		return nil, ErrAlreadyExpiredSessionId
	}

	session.LoginInfo = &model.LoginInfo{
		UserIP:    sessionEntity.LoginSession.UserIP,
		UserAgent: sessionEntity.LoginSession.UserAgent,
		Platform:  sessionEntity.LoginSession.Platform,
		Version:   sessionEntity.LoginSession.Version,
		Device:    sessionEntity.LoginSession.Device,
	}
	session.CreatedAt = sessionEntity.CreatedAt
	session.ExpiredAt = sessionEntity.ExpiredAt
	session.Signature = ""

	return session, nil
}

func (s *Service) buildTokenClaims(session *model.Session) *model.TokenClaims {
	return &model.TokenClaims{
		Issuer:    s.appConfig.GetServiceName(),
		UserId:    session.UserId,
		LoginInfo: session.LoginInfo,
		CreatedAt: &jwt.NumericDate{Time: time.Unix(session.CreatedAt, 0)},
		ExpiredAt: &jwt.NumericDate{Time: time.Unix(session.ExpiredAt, 0)},
		Signature: session.Signature,
	}
}

func (s *Service) buildSession(tokenClaims *model.TokenClaims) *model.Session {
	return &model.Session{
		UserId:    tokenClaims.UserId,
		LoginInfo: tokenClaims.LoginInfo,
		CreatedAt: tokenClaims.CreatedAt.Unix(),
		ExpiredAt: tokenClaims.ExpiredAt.Unix(),
		Signature: tokenClaims.Signature,
	}
}

func (s *Service) delExpiredSessionInBg(ctx context.Context, userId, platform string) {
	simpleBgObserver := s.observer.StartInBg(ctx, "del-expired-auth-in-bg")
	defer simpleBgObserver.End()

	bgObserverCtx := simpleBgObserver.Ctx()

	s.cache.DelSession(bgObserverCtx, userId, platform)
}
