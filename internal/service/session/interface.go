package session_service

import (
	"context"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

type IService interface {
	CreateSession(ctx context.Context, userId string, loginInfo *model.LoginInfo) (string, error)
	GetSession(ctx context.Context, sessionId string) (*model.Session, error)
}
