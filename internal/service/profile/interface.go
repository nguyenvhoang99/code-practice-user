package profile_service

import (
	"context"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
)

type IService interface {
	QueryProfileByUserId(ctx context.Context, userId string, queryOption *model.QueryProfileOption) (*model.Profile, error)
	QueryProfileByEmail(ctx context.Context, email string, queryOption *model.QueryProfileOption) (*model.Profile, error)
	QueryProfileByPhone(ctx context.Context, phone string, queryOption *model.QueryProfileOption) (*model.Profile, error)

	QueryProfilesByUserIds(ctx context.Context, userIds []string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error)
	QueryProfilesByEmails(ctx context.Context, emails []string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error)
	QueryProfilesByPhones(ctx context.Context, phones []string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error)

	QueryProfileIdsByBindingAppEmail(ctx context.Context, bindingAppName, bindingAppEmail string) (*model.Profile, error)
	QueryProfileIdsByBindingAppPhone(ctx context.Context, bindingAppName, bindingAppPhone string) (*model.Profile, error)
}
