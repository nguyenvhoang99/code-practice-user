package profile_service

import (
	"context"
	"errors"
	"sync"

	custom_err "gitlab.com/nguyenvhoang99/common-pkg/pkg/err"
	pkg_model "gitlab.com/nguyenvhoang99/common-pkg/pkg/model"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/observer"

	"gitlab.com/nguyenvhoang99/code-practice-user/internal/model"
	profile_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/profile"
)

var (
	ErrEmptyUserId = &custom_err.InvalidErr{Err: "empty userId"}
	ErrEmptyEmail  = &custom_err.InvalidErr{Err: "empty email"}
	ErrEmptyPhone  = &custom_err.InvalidErr{Err: "empty phone"}
)

type Service struct {
	cache    profile_repository.ICache
	database profile_repository.IDatabase
	observer observer.IObserver
}

func NewService(cache profile_repository.ICache, database profile_repository.IDatabase, observer observer.IObserver) IService {
	return &Service{
		cache:    cache,
		database: database,
		observer: observer,
	}
}

func (s *Service) QueryProfileByUserId(ctx context.Context, userId string, queryOption *model.QueryProfileOption) (*model.Profile, error) {
	if userId == "" {
		return nil, ErrEmptyUserId
	}

	cacheProfileIdsChan := make(chan *pkg_model.DataAndErr[*model.ProfileIds])
	go s.getCacheProfileIdsByUserIdAsync(ctx, userId, cacheProfileIdsChan)

	cacheBasicProfileChan := make(chan *pkg_model.DataAndErr[*model.BasicProfile])
	if queryOption.WithBasicProfile {
		go s.getCacheBasicProfileByUserIdAsync(ctx, userId, cacheBasicProfileChan)
	}

	cacheBindingProfilesChan := make(chan *pkg_model.DataAndErr[map[string]*model.BindingProfile])
	if queryOption.WithAllBindingProfiles || len(queryOption.BindingAppNames) > 0 {
		go s.getCacheBindingProfileByUserIdAsync(ctx, userId, queryOption.WithAllBindingProfiles, queryOption.BindingAppNames, cacheBindingProfilesChan)
	}

	cacheProfileIds := <-cacheProfileIdsChan

	var cacheBasicProfile *pkg_model.DataAndErr[*model.BasicProfile]
	if queryOption.WithBasicProfile {
		cacheBasicProfile = <-cacheBasicProfileChan
	}

	var cacheBindingProfiles *pkg_model.DataAndErr[map[string]*model.BindingProfile]
	if queryOption.WithAllBindingProfiles || len(queryOption.BindingAppNames) > 0 {
		cacheBindingProfiles = <-cacheBindingProfilesChan
	}

	queryDatabase := false
	queryDatabaseOption := &model.QueryProfileOption{}

	profile := &model.Profile{
		UserId: userId,
	}

	if cacheProfileIds != nil && cacheProfileIds.Data != nil {
		profile.Email = cacheProfileIds.Data.Email
		profile.Phone = cacheProfileIds.Data.Phone
	} else {
		queryDatabase = true
	}

	if queryOption.WithBasicProfile {
		if cacheBasicProfile != nil && cacheBasicProfile.Data != nil {
			profile.BasicProfile = cacheBasicProfile.Data
		} else {
			queryDatabase = true
			queryDatabaseOption.WithBasicProfile = true
		}
	}

	if queryOption.WithAllBindingProfiles || len(queryOption.BindingAppNames) > 0 {
		if cacheBindingProfiles != nil && cacheBindingProfiles.Data != nil {
			profile.BindingProfiles = cacheBindingProfiles.Data
		} else {
			queryDatabase = true
			queryDatabaseOption.WithAllBindingProfiles = queryOption.WithAllBindingProfiles
			queryDatabaseOption.BindingAppNames = queryOption.BindingAppNames
		}
	}

	if !queryDatabase {
		return profile, nil
	}

	return s.warmupProfile(ctx, &model.ProfileIds{UserId: userId}, profile, queryDatabaseOption)
}

func (s *Service) QueryProfileByEmail(ctx context.Context, email string, queryOption *model.QueryProfileOption) (*model.Profile, error) {
	if email == "" {
		return nil, ErrEmptyEmail
	}

	cacheProfileIds, err := s.cache.GetProfileIdsByEmail(ctx, email)
	if err == nil && cacheProfileIds != nil && cacheProfileIds.UserId != "" {
		return s.QueryProfileByUserId(ctx, cacheProfileIds.UserId, queryOption)
	}

	return s.warmupProfile(ctx, &model.ProfileIds{Email: email}, nil, queryOption)
}

func (s *Service) QueryProfileByPhone(ctx context.Context, phone string, queryOption *model.QueryProfileOption) (*model.Profile, error) {
	if phone == "" {
		return nil, ErrEmptyPhone
	}

	cacheProfileIds, err := s.cache.GetProfileIdsByPhone(ctx, phone)
	if err == nil && cacheProfileIds != nil && cacheProfileIds.UserId != "" {
		return s.QueryProfileByUserId(ctx, cacheProfileIds.UserId, queryOption)
	}

	return s.warmupProfile(ctx, &model.ProfileIds{Phone: phone}, nil, queryOption)
}

func (s *Service) QueryProfilesByUserIds(ctx context.Context, userIds []string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error) {
	wg := &sync.WaitGroup{}
	profilesChan := make(chan *pkg_model.DataAndErr[*model.Profile])

	for _, userId := range userIds {
		if userId == "" {
			continue
		}

		wg.Add(1)
		go func(ctx context.Context, userId string, profilesChan chan *pkg_model.DataAndErr[*model.Profile], wg *sync.WaitGroup) {
			defer wg.Done()

			profile, err := s.QueryProfileByUserId(ctx, userId, queryOption)
			if err != nil {
				profilesChan <- &pkg_model.DataAndErr[*model.Profile]{
					Data: nil,
					Err:  err,
				}
				return
			}

			profilesChan <- &pkg_model.DataAndErr[*model.Profile]{
				Data: profile,
				Err:  nil,
			}
		}(ctx, userId, profilesChan, wg)
	}

	go func() {
		wg.Wait()
		close(profilesChan)
	}()

	profiles := make(map[string]*model.Profile)
	for profile := range profilesChan {
		if profile.Err != nil {
			return nil, profile.Err
		}
		if profile.Data == nil {
			continue
		}
		profiles[profile.Data.UserId] = profile.Data
	}
	return profiles, nil
}

func (s *Service) QueryProfilesByEmails(ctx context.Context, emails []string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error) {
	wg := &sync.WaitGroup{}
	profilesChan := make(chan *pkg_model.DataAndErr[*model.Profile])

	for _, email := range emails {
		if email == "" {
			continue
		}

		wg.Add(1)
		go func(ctx context.Context, email string, profilesChan chan *pkg_model.DataAndErr[*model.Profile], wg *sync.WaitGroup) {
			defer wg.Done()

			profile, err := s.QueryProfileByEmail(ctx, email, queryOption)
			if err != nil {
				profilesChan <- &pkg_model.DataAndErr[*model.Profile]{
					Data: nil,
					Err:  err,
				}
				return
			}

			profilesChan <- &pkg_model.DataAndErr[*model.Profile]{
				Data: profile,
				Err:  nil,
			}
		}(ctx, email, profilesChan, wg)
	}

	go func() {
		wg.Wait()
		close(profilesChan)
	}()

	profiles := make(map[string]*model.Profile)
	for profile := range profilesChan {
		if profile.Err != nil {
			return nil, profile.Err
		}
		if profile.Data == nil {
			continue
		}
		profiles[profile.Data.Email] = profile.Data
	}
	return profiles, nil
}

func (s *Service) QueryProfilesByPhones(ctx context.Context, phones []string, queryOption *model.QueryProfileOption) (map[string]*model.Profile, error) {
	wg := &sync.WaitGroup{}
	profilesChan := make(chan *pkg_model.DataAndErr[*model.Profile])

	for _, phone := range phones {
		if phone == "" {
			continue
		}

		wg.Add(1)
		go func(ctx context.Context, phone string, profilesChan chan *pkg_model.DataAndErr[*model.Profile], wg *sync.WaitGroup) {
			defer wg.Done()

			profile, err := s.QueryProfileByPhone(ctx, phone, queryOption)
			if err != nil {
				profilesChan <- &pkg_model.DataAndErr[*model.Profile]{
					Data: nil,
					Err:  err,
				}
				return
			}

			profilesChan <- &pkg_model.DataAndErr[*model.Profile]{
				Data: profile,
				Err:  nil,
			}
		}(ctx, phone, profilesChan, wg)
	}

	go func() {
		wg.Wait()
		close(profilesChan)
	}()

	profiles := make(map[string]*model.Profile)
	for profile := range profilesChan {
		if profile.Err != nil {
			return nil, profile.Err
		}
		if profile.Data == nil {
			continue
		}
		profiles[profile.Data.Phone] = profile.Data
	}
	return profiles, nil
}

func (s *Service) QueryProfileIdsByBindingAppEmail(ctx context.Context, bindingAppName, bindingAppEmail string) (*model.Profile, error) {
	databaseProfile, err := s.database.FindProfileByBindingId(ctx, &model.ProfileBindingIds{
		AppName: bindingAppName,
		Email:   bindingAppEmail,
	}, &model.QueryProfileOption{
		WithBasicProfile:       false,
		WithAllBindingProfiles: false,
		BindingAppNames:        make([]string, 0),
	})
	if err != nil {
		if errors.Is(err, profile_repository.ErrInactiveUser) {
			return nil, err
		}
		return nil, err
	}
	return databaseProfile, nil
}

func (s *Service) QueryProfileIdsByBindingAppPhone(ctx context.Context, bindingAppName, bindingAppPhone string) (*model.Profile, error) {
	databaseProfile, err := s.database.FindProfileByBindingId(ctx, &model.ProfileBindingIds{
		AppName: bindingAppName,
		Phone:   bindingAppPhone,
	}, &model.QueryProfileOption{
		WithBasicProfile:       false,
		WithAllBindingProfiles: false,
		BindingAppNames:        make([]string, 0),
	})
	if err != nil {
		if errors.Is(err, profile_repository.ErrInactiveUser) {
			return nil, err
		}
		return nil, err
	}
	return databaseProfile, nil
}

func (s *Service) getCacheProfileIdsByUserIdAsync(ctx context.Context, userId string, profileIdsChan chan *pkg_model.DataAndErr[*model.ProfileIds]) {
	profileIds, err := s.cache.GetProfileIdsByUserId(ctx, userId)
	if err != nil {
		profileIdsChan <- &pkg_model.DataAndErr[*model.ProfileIds]{
			Data: nil,
			Err:  err,
		}
		return
	}

	profileIdsChan <- &pkg_model.DataAndErr[*model.ProfileIds]{
		Data: profileIds,
		Err:  nil,
	}
}

func (s *Service) getCacheBasicProfileByUserIdAsync(ctx context.Context, userId string, basicProfileChan chan *pkg_model.DataAndErr[*model.BasicProfile]) {
	basicProfile, err := s.cache.GetBasicProfile(ctx, userId)
	if err != nil {
		basicProfileChan <- &pkg_model.DataAndErr[*model.BasicProfile]{
			Data: nil,
			Err:  err,
		}
		return
	}

	basicProfileChan <- &pkg_model.DataAndErr[*model.BasicProfile]{
		Data: basicProfile,
		Err:  nil,
	}
}

func (s *Service) getCacheBindingProfileByUserIdAsync(ctx context.Context, userId string, all bool, appNames []string, bindingProfilesChan chan *pkg_model.DataAndErr[map[string]*model.BindingProfile]) {
	bindingProfile, err := s.cache.GetBindingProfiles(ctx, userId, all, appNames)
	if err != nil {
		bindingProfilesChan <- &pkg_model.DataAndErr[map[string]*model.BindingProfile]{
			Data: nil,
			Err:  err,
		}
		return
	}

	bindingProfilesChan <- &pkg_model.DataAndErr[map[string]*model.BindingProfile]{
		Data: bindingProfile,
		Err:  nil,
	}
}

func (s *Service) warmupProfile(ctx context.Context, profileIds *model.ProfileIds, profile *model.Profile, queryDatabaseOption *model.QueryProfileOption) (*model.Profile, error) {
	databaseProfile, err := s.database.FindProfileById(ctx, profileIds, queryDatabaseOption)
	if err != nil {
		if errors.Is(err, profile_repository.ErrInactiveUser) {
			return nil, err
		}
		return nil, err
	}
	if databaseProfile == nil {
		return nil, nil
	}

	go s.setCacheProfileIdsInBg(ctx, &model.ProfileIds{
		UserId: databaseProfile.UserId,
		Email:  databaseProfile.Email,
		Phone:  databaseProfile.Phone,
	})

	if databaseProfile.BasicProfile == nil {
		databaseProfile.BasicProfile = &model.BasicProfile{}
	}
	go s.setCacheBasicProfileInBg(ctx, databaseProfile.UserId, databaseProfile.BasicProfile)

	if len(databaseProfile.BindingProfiles) == 0 {
		databaseProfile.BindingProfiles = make(map[string]*model.BindingProfile)
	}
	go s.setCacheBindingProfilesInBg(ctx, databaseProfile.UserId, databaseProfile.BindingProfiles)

	if profile == nil {
		profile = &model.Profile{
			UserId: databaseProfile.UserId,
			Email:  databaseProfile.Email,
			Phone:  databaseProfile.Phone,
		}
	} else {
		profile.UserId = databaseProfile.UserId
		profile.Email = databaseProfile.Email
		profile.Phone = databaseProfile.Phone
	}

	if queryDatabaseOption.WithBasicProfile && profile.BasicProfile == nil {
		profile.BasicProfile = databaseProfile.BasicProfile
	}

	if (queryDatabaseOption.WithAllBindingProfiles || len(queryDatabaseOption.BindingAppNames) > 0) && len(profile.BindingProfiles) == 0 {
		profile.BindingProfiles = databaseProfile.BindingProfiles
	}

	return profile, nil
}

func (s *Service) setCacheProfileIdsInBg(ctx context.Context, profileIds *model.ProfileIds) {
	simpleBgObserver := s.observer.StartInBg(ctx, "set-cache-profile-ids-in-bg")
	defer simpleBgObserver.End()

	bgObserverCtx := simpleBgObserver.Ctx()

	wg := &sync.WaitGroup{}

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		err := s.cache.SetProfileIdsByUserId(bgObserverCtx, profileIds.UserId, profileIds)
		if err != nil {
			simpleBgObserver.WithErr(err)
		}
	}(wg)

	if profileIds.Email != "" {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			defer wg.Done()
			err := s.cache.SetProfileIdsByEmail(bgObserverCtx, profileIds.Email, profileIds)
			if err != nil {
				simpleBgObserver.WithErr(err)
			}
		}(wg)
	}

	if profileIds.Phone != "" {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			defer wg.Done()
			err := s.cache.SetProfileIdsByPhone(bgObserverCtx, profileIds.Phone, profileIds)
			if err != nil {
				simpleBgObserver.WithErr(err)
			}
		}(wg)
	}

	wg.Wait()
}

func (s *Service) setCacheBasicProfileInBg(ctx context.Context, userId string, basicProfile *model.BasicProfile) {
	simpleBgObserver := s.observer.StartInBg(ctx, "set-cache-basic-profile-in-bg")
	defer simpleBgObserver.End()

	bgObserverCtx := simpleBgObserver.Ctx()

	err := s.cache.SetBasicProfile(bgObserverCtx, userId, basicProfile)
	if err != nil {
		simpleBgObserver.WithErr(err)
	}
}

func (s *Service) setCacheBindingProfilesInBg(ctx context.Context, userId string, bindingProfiles map[string]*model.BindingProfile) {
	simpleBgObserver := s.observer.StartInBg(ctx, "set-cache-binding-profiles-in-bg")
	defer simpleBgObserver.End()

	bgObserverCtx := simpleBgObserver.Ctx()

	err := s.cache.SetBindingProfiles(bgObserverCtx, userId, bindingProfiles)
	if err != nil {
		simpleBgObserver.WithErr(err)
	}
}
