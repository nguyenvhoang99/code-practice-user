package profile_service_fx

import (
	"go.uber.org/fx"

	profile_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/profile"
)

var ServiceModule = fx.Provide(profile_service.NewService)
