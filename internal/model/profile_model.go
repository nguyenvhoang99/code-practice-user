package model

type QueryProfileOption struct {
	WithBasicProfile       bool
	WithAllBindingProfiles bool
	BindingAppNames        []string
}

type ProfileIds struct {
	UserId string
	Email  string
	Phone  string
}

type ProfileBindingIds struct {
	AppName string
	Email   string
	Phone   string
}

type Profile struct {
	UserId          string                     `json:"user_id,omitempty"`
	Email           string                     `json:"email,omitempty"`
	Phone           string                     `json:"phone,omitempty"`
	BasicProfile    *BasicProfile              `json:"basic_profile,omitempty"`
	BindingProfiles map[string]*BindingProfile `json:"binding_profiles,omitempty"`
}

type BasicProfile struct {
	Name   string `json:"name,omitempty"`
	Avatar string `json:"avatar,omitempty"`
}

type BindingProfile struct {
	AppName   string `json:"app_name,omitempty"`
	AppUserId string `json:"app_user_id,omitempty"`
	AppEmail  string `json:"app_email,omitempty"`
	AppPhone  string `json:"app_phone,omitempty"`
}
