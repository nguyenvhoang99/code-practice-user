package model

import (
	"crypto/sha256"
	"strconv"

	"github.com/btcsuite/btcutil/base58"
	"github.com/golang-jwt/jwt/v5"

	pkg_constants "gitlab.com/nguyenvhoang99/common-pkg/pkg/constants"
)

type Session struct {
	UserId    string
	LoginInfo *LoginInfo
	CreatedAt int64
	ExpiredAt int64
	Signature string
}

type LoginInfo struct {
	UserIP    string
	UserAgent string
	Platform  string
	Version   string
	Device    string
}

func (s *Session) BuildSignature() (string, error) {
	signatureInput := s.UserId +
		pkg_constants.Delimiter + s.LoginInfo.Platform +
		pkg_constants.Delimiter + s.LoginInfo.Device +
		pkg_constants.Delimiter + strconv.FormatInt(s.CreatedAt, 10) +
		pkg_constants.Delimiter + strconv.FormatInt(s.ExpiredAt, 10)

	hash := sha256.New()
	_, err := hash.Write([]byte(signatureInput))
	if err != nil {
		return "", err
	}
	signatureOutput := hash.Sum([]byte(s.UserId))

	signature := base58.Encode(signatureOutput)
	return signature, nil
}

type TokenClaims struct {
	Issuer    string           `json:"iss"`
	UserId    string           `json:"sub"`
	LoginInfo *LoginInfo       `json:"log"`
	CreatedAt *jwt.NumericDate `json:"iat"`
	ExpiredAt *jwt.NumericDate `json:"exp"`
	Signature string           `json:"sig"`
}

func (t *TokenClaims) GetIssuer() (string, error) {
	return t.Issuer, nil
}

func (t *TokenClaims) GetSubject() (string, error) {
	return t.UserId, nil
}

func (t *TokenClaims) GetAudience() (jwt.ClaimStrings, error) {
	return nil, nil
}

func (t *TokenClaims) GetIssuedAt() (*jwt.NumericDate, error) {
	return t.CreatedAt, nil
}

func (t *TokenClaims) GetNotBefore() (*jwt.NumericDate, error) {
	return t.CreatedAt, nil
}

func (t *TokenClaims) GetExpirationTime() (*jwt.NumericDate, error) {
	return t.ExpiredAt, nil
}
