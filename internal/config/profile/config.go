package profile_config

import (
	"fmt"

	"github.com/spf13/viper"

	pkg_constants "gitlab.com/nguyenvhoang99/common-pkg/pkg/constants"
)

const configKey = "profile"

func NewConfig() (*Config, error) {
	if !viper.InConfig(configKey) {
		return nil, fmt.Errorf("%s %s", pkg_constants.NotExistedConfigMsg, configKey)
	}

	config := &Config{}
	if err := viper.UnmarshalKey(configKey, config); err != nil {
		return nil, err
	}

	if config.CacheTTLInDay <= 0 {
		return nil, fmt.Errorf("%s %s: cache-ttl-in-day=%d", pkg_constants.InvalidConfigMsg, configKey, config.CacheTTLInDay)
	}
	return config, nil
}
