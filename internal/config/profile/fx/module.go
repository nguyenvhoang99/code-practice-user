package profile_config_fx

import (
	"go.uber.org/fx"

	profile_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/config/profile"
)

var ConfigModule = fx.Provide(profile_config.NewConfig)
