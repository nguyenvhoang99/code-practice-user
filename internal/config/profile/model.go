package profile_config

type Config struct {
	CacheTTLInDay int16 `mapstructure:"cache-ttl-in-day"`
}
