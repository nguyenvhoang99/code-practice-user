package session_config

type Config struct {
	SignKey        string `mapstructure:"sign-key"`
	SignKeyByteArr []byte
	AliveInHour    int16 `mapstructure:"alive-in-hour"`
	CacheTTLInHour int16 `mapstructure:"cache-ttl-in-hour"`
}
