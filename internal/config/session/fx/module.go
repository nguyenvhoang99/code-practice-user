package session_config_fx

import (
	"go.uber.org/fx"

	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/config/session"
)

var ConfigModule = fx.Provide(session_config.NewConfig)
