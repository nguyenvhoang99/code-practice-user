package session_config

import (
	"fmt"

	"github.com/spf13/viper"

	pkg_constants "gitlab.com/nguyenvhoang99/common-pkg/pkg/constants"
)

const configKey = "session"

func NewConfig() (*Config, error) {
	if !viper.InConfig(configKey) {
		return nil, fmt.Errorf("%s %s", pkg_constants.NotExistedConfigMsg, configKey)
	}

	config := &Config{}
	if err := viper.UnmarshalKey(configKey, config); err != nil {
		return nil, err
	}

	if config.SignKey == "" {
		return nil, fmt.Errorf("%s %s: sign-key=%s", pkg_constants.InvalidConfigMsg, configKey, config.SignKey)
	}
	config.SignKeyByteArr = []byte(config.SignKey)
	if config.AliveInHour <= 0 {
		return nil, fmt.Errorf("%s %s: alive-in-hour=%d", pkg_constants.InvalidConfigMsg, configKey, config.AliveInHour)
	}
	if config.CacheTTLInHour <= 0 {
		return nil, fmt.Errorf("%s %s: cache-ttl-in-hour=%d", pkg_constants.InvalidConfigMsg, configKey, config.CacheTTLInHour)
	}
	return config, nil
}
