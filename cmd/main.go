package main

import (
	"log"

	"go.uber.org/fx"

	app "gitlab.com/nguyenvhoang99/common-pkg/pkg/app/fx"
	mongodb_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/mongodb/fx"
	postgresql_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/postgresql/fx"
	redis_database "gitlab.com/nguyenvhoang99/common-pkg/pkg/database/redis/fx"
	"gitlab.com/nguyenvhoang99/common-pkg/pkg/loader"
	observer "gitlab.com/nguyenvhoang99/common-pkg/pkg/observer/fx"
	grpc_server "gitlab.com/nguyenvhoang99/common-pkg/pkg/server/grpc/fx"
	http_server "gitlab.com/nguyenvhoang99/common-pkg/pkg/server/http/fx"

	profile_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/config/profile/fx"
	session_config "gitlab.com/nguyenvhoang99/code-practice-user/internal/config/session/fx"
	grpc_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc/fx"
	profile_grpc_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc/profile/fx"
	session_grpc_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/grpc/session/fx"
	auth_http_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/auth/fx"
	http_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/fx"
	group_http_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/group/fx"
	profile_http_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/profile/fx"
	session_http_handler "gitlab.com/nguyenvhoang99/code-practice-user/internal/handler/http/session/fx"
	auth_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/auth/fx"
	group_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/group/fx"
	profile_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/profile/fx"
	session_repository "gitlab.com/nguyenvhoang99/code-practice-user/internal/repository/session/fx"
	group_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/group/fx"
	profile_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/profile/fx"
	session_service "gitlab.com/nguyenvhoang99/code-practice-user/internal/service/session/fx"
)

func main() {
	err := loader.NewConfigLoader()
	if err != nil {
		log.Fatalf("failed to init configs loader: err=%s", err.Error())
	}

	service := fx.New(
		app.ConfigModule,

		observer.ObserverModule,

		postgresql_database.DatabaseModule,
		mongodb_database.DatabaseModule,
		redis_database.DatabaseModule,

		grpc_server.ServerConfigModule,
		grpc_server.ServerModule,
		http_server.ServerConfigModule,
		http_server.ServerModule,

		auth_repository.DatabaseModule,
		auth_http_handler.HandlerModule,

		session_config.ConfigModule,
		session_repository.CacheModule,
		session_service.ServiceModule,
		session_grpc_handler.HandlerModule,
		session_http_handler.HandlerModule,

		profile_config.ConfigModule,
		profile_repository.DatabaseModule,
		profile_repository.CacheModule,
		profile_service.ServiceModule,
		profile_grpc_handler.HandlerModule,
		profile_http_handler.HandlerModule,

		group_repository.DatabaseModule,
		group_service.ServiceModule,
		group_http_handler.HttpHandlerModule,

		grpc_handler.HandlerModule,
		http_handler.HandlerModule,
	)
	service.Run()
}
